int: R;
set of int: ROUNDS = 0..R - 1;
set of int: MAIN_ROUNDS = 0..R - 2;
set of int: COLS = 0..3;
set of int: ROWS = 0..3;
array[ROUNDS, ROWS, COLS] of var bool: DXupper;
array[ROUNDS, ROWS, COLS] of var bool: freeXupper;
array[ROUNDS, ROWS, COLS] of var bool: freeSBupper;
array[ROUNDS, ROWS, COLS] of var bool: DXlower;
array[ROUNDS, ROWS, COLS] of var bool: freeXlower;
array[ROUNDS, ROWS, COLS] of var bool: freeSBlower;
array[ROUNDS, ROWS, COLS] of var bool: isDDT2;
array[ROUNDS, ROWS, COLS] of var bool: isTable;
var int: objective;

% Constraint on free upper variables 
constraint forall(r in MAIN_ROUNDS, k in COLS) (
  freeXupper[r + 1, 0, k] = (freeSBupper[r, 0, k] \/ freeSBupper[r, 2, (k + 2) mod 4] \/ freeSBupper[r, 3, (k + 1) mod 4]) /\
  freeXupper[r + 1, 1, k] = freeSBupper[r, 0, k] /\
  freeXupper[r + 1, 2, k] = (freeSBupper[r, 1, (k + 3) mod 4] \/ freeSBupper[r, 2, (k + 2) mod 4]) /\
  freeXupper[r + 1, 3, k] = (freeSBupper[r, 0, k] \/ freeSBupper[r, 2, (k + 2) mod 4])
);

% Constraint on free lower variables 
constraint forall(r in MAIN_ROUNDS, k in COLS) (
  freeSBlower[r, 0, k] = freeXlower[r + 1, 1, k] /\
  freeSBlower[r, 1, k] = (freeXlower[r + 1, 1, (k + 1) mod 4] \/ freeXlower[r + 1, 2, (k + 1) mod 4] \/ freeXlower[r + 1, 3, (k + 1) mod 4]) /\
  freeSBlower[r, 2, k] = (freeXlower[r + 1, 1, (k + 2) mod 4] \/ freeXlower[r + 1, 3, (k + 2) mod 4]) /\
  freeSBlower[r, 3, k] = (freeXlower[r + 1, 0, (k + 3) mod 4] \/ freeXlower[r + 1, 3, (k + 3) mod 4])
);

constraint forall(r in ROUNDS, j in ROWS, k in COLS) (
  one_objective_constraint(
    DXupper[r, j, k], freeXupper[r, j, k], freeSBupper[r, j, k],
    DXlower[r, j, k], freeXlower[r, j, k], freeSBlower[r, j, k],
    isTable[r, j, k], isDDT2[r, j, k]
  )
);

constraint sum(r in ROUNDS, j in ROWS, k in COLS) (isTable[r, j, k] + isDDT2[r, j, k]) = objective;

predicate one_objective_constraint(var bool: DXupper, var bool: freeXupper, var bool: freeSBupper, var bool: DXlower, var bool: freeXlower, var bool: freeSBlower, var bool: isTable, var bool: isDDT2) =
  (freeSBupper -> DXupper) /\
  (freeXlower -> DXlower) /\
  (freeXupper -> freeSBupper) /\
  (freeSBlower -> freeXlower) /\
  (isDDT2 -> isTable) /\
  (not(freeSBupper) \/ not(freeXlower) \/ not(isTable)) /\
  (DXupper -> (freeSBupper \/ isTable)) /\
  (DXlower -> (freeXlower \/ isTable)) /\
  (DXupper \/ not(freeXlower) \/ not(isTable)) /\
  (DXlower \/ not(freeSBupper) \/ not(isTable)) /\
  (isTable -> (DXupper \/ DXlower)) /\
  (isDDT2 \/ not(freeXupper) \/ not(DXlower)) /\
  (isDDT2 \/ not(freeSBlower) \/ not(DXupper)) /\
  (isDDT2 -> (freeXupper \/ freeSBlower))
;

% Add known diff bounds for DKupper
constraint forall(start in ROUNDS) (
    forall(end in start + 4..R - 1) (
        sum(round in start..end, j in ROWS, k in COLS) (DXupper[round, j, k]) >= diff_bounds[end - start + 1]
    )
);

% Add known diff bounds for DKlower
constraint forall(start in ROUNDS) (
    forall(end in start + 4..R - 1) (
        sum(round in start..end, j in ROWS, k in COLS) (DXlower[round, j, k]) >= diff_bounds[end - start + 1]
    )
);

