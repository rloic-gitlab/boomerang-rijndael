int: Nr;
int: BLOCK_BITS;

function set of int: range(int: start, int: exclusive_end) = start..exclusive_end - 1;

int: Nb = BLOCK_BITS div 32;
int: SHIFT_ROW_INDEX = Nb - 4;
set of int: ROUNDS = range(0, Nr);
set of int: MAIN_ROUNDS = range(0, Nr - 1);
set of int: ATTACK_ROUNDS = range(1, Nr - 1);
set of int: BLOCK_COLS = range(0, Nb);
set of int: ROWS = range(0, 4);
var 0..256: objective;
constraint not(objective in {1, 2, 3, 4, 7, 8, 9, 13, 14, 19});
var int: number_of_active_sboxes;

array[0..4, ROWS] of int: SHIFT_ROWS = array2d(0..4, ROWS,
    [|0, 1, 2, 3 % Keysize = 128
     |0, 1, 2, 3 % Keysize = 160
     |0, 1, 2, 3 % Keysize = 192
     |0, 1, 2, 4 % Keysize = 224
     |0, 1, 3, 4|] % Keysize = 256
);

function var bool: isDDT(int: i, int: j, int: k) = (isDDT_upper(i, j, k) \/ isDDT_lower(i, j, k));

function var bool: isDDT_upper(int: i, int: j, int: k) = (
    (DXupper[i, j, k] /\ not(freeXupper[i, j, k]) /\ not(freeSBupper[i, j, k]) /\ not(DXlower[i, j, k]))
);

function var bool: isDDT_lower(int: i, int: j, int: k) = (
    (not(DXupper[i, j, k]) /\ DXlower[i, j, k] /\ not(freeXlower[i, j, k]) /\ not(freeSBlower[i, j, k]))
);

function var bool: isBCT(int: i, int: j, int: k) = (
    DXupper[i, j, k] /\ not(freeXupper[i, j, k]) /\ freeSBupper[i, j, k] /\ DXlower[i, j, k] /\ freeXlower[i, j, k] /\ not(freeSBlower[i, j, k])
);

function var bool: isDDT2(int: i, int: j, int: k) = (isDDT2_upper(i, j, k) \/ isDDT2_lower(i, j, k));

function var bool: isDDT2_upper(int: i, int: j, int: k) = (
    (DXupper[i, j, k] /\ not(freeXupper[i, j, k]) /\ not(freeSBupper[i, j, k]) /\ DXlower[i, j, k] /\ freeXlower[i, j, k] /\ freeSBlower[i, j, k])
);

function var bool: isDDT2_lower(int: i, int: j, int: k) = (
    (DXupper[i, j, k] /\ freeXupper[i, j, k] /\ freeSBupper[i, j, k] /\ DXlower[i, j, k] /\ not(freeXlower[i, j, k]) /\ not(freeSBlower[i, j, k]))
);

function var bool: isUBCT(int: i, int: j, int: k) = (
    DXupper[i, j, k] /\ not(freeXupper[i, j, k]) /\ not(freeSBupper[i, j, k]) /\ DXlower[i, j, k] /\ freeXlower[i, j, k] /\ not(freeSBlower[i, j, k])
);

function var bool: isLBCT(int: i, int: j, int: k) = (
    DXupper[i, j, k] /\ not(freeXupper[i, j, k]) /\ freeSBupper[i, j, k] /\ DXlower[i, j, k] /\ not(freeXlower[i, j, k]) /\ not(freeSBlower[i, j, k])
);

function var bool: isEBCT(int: i, int: j, int: k) = (
    DXupper[i, j, k] /\ not(freeXupper[i, j, k]) /\ not(freeSBupper[i, j, k]) /\ DXlower[i, j, k] /\ not(freeXlower[i, j, k]) /\ not(freeSBlower[i, j, k])
);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Upper trail
array [ROUNDS, ROWS, BLOCK_COLS] of var bool: DXupper;
array [ROUNDS, ROWS, BLOCK_COLS] of var bool: freeXupper;

function var bool: DSBupper(int: i, int: j, int: k) = DXupper[i, j, k];
array [ROUNDS, ROWS, BLOCK_COLS] of var bool: freeSBupper;

function var bool: DYupper(int: i, int: j, int: k) = DSBupper(i, j, (k + SHIFT_ROWS[SHIFT_ROW_INDEX, j]) mod Nb);
function var bool: freeYupper(int: i, int: j, int: k) = freeSBupper[i, j, (k + SHIFT_ROWS[SHIFT_ROW_INDEX, j]) mod Nb];
array [MAIN_ROUNDS, BLOCK_COLS, ROWS, MAIN_ROUNDS, BLOCK_COLS] of var bool: diffYupper;

array [MAIN_ROUNDS, ROWS, BLOCK_COLS] of var bool: DZupper;
array [MAIN_ROUNDS, ROWS, BLOCK_COLS] of var bool: freeZupper;
array [MAIN_ROUNDS, BLOCK_COLS, ROWS, MAIN_ROUNDS, BLOCK_COLS] of var bool: diffZupper;

constraint forall(j in ROWS, k in BLOCK_COLS) (
    DYupper(0, j, k) = (DZupper[0, 0, k] \/ DZupper[0, 1, k] \/ DZupper[0, 2, k] \/ DZupper[0, 3, k])
);

var int: rb;
constraint sum(j in ROWS, k in BLOCK_COLS) (DYupper(0, j, k) * 8) = rb;
var int: tb;
constraint sum(j in ROWS, k in BLOCK_COLS) (DYupper(0, j, k) * 7) = tb;

var int: rf;
constraint sum(j in ROWS, k in BLOCK_COLS) (DXlower[Nr - 1, j, k] * 8) = rf;
var int: tf;
constraint sum(j in ROWS, k in BLOCK_COLS) (DXlower[Nr - 1, j, k] * 7) = tf;

% ARK for lower trail
constraint forall(j in ROWS, k in BLOCK_COLS) (
    DXlower[Nr - 1, j, k] = (DZlower[Nr - 2, j, k] \/ SubKeyLower(Nr - 1, j, k))
);

var int: rb_rf;

constraint rb + rf = rb_rf;

var int: TWO_T; % For the equations: p^2 * q^2 * r = 2^-2t <=> 2t = -log2(p^2 * q^2 * r)

% var bool: ATTACK_I;
% var bool: ATTACK_II;
% var bool: ATTACK_III;

int: sigma = 1; % s = 4 = 2^{2sigma} <=> sigma = 1
var int: time_complexity;
constraint minimum(time_complexity, [
    attack_I_time_complexity,
    attack_II_time_complexity,
    attack_III_time_complexity,
]);

var int: attack_I_time_complexity;
constraint attack_I_time_complexity = rf + 2 + sigma + ceiling_div(BLOCK_BITS, 2) + ceiling_div(TWO_T, 2) + rb;

var int: attack_II_time_complexity_0;
var int: attack_II_time_complexity_1;
var int: attack_II_time_complexity_2;
var int: attack_II_time_complexity_3;
var int: attack_II_time_complexity_4;
var int: attack_II_time_complexity_5;

constraint attack_II_time_complexity_0 = rf - 1 + 2 * sigma + TWO_T;
constraint attack_II_time_complexity_1 = tf + 2 * sigma + TWO_T;
constraint attack_II_time_complexity_2 = 2 * tf + 2 * rb - BLOCK_BITS - 2 + 2 * sigma + TWO_T;
constraint attack_II_time_complexity_3 = rb + tb + 2 * tf - BLOCK_BITS - 1 + 2 * sigma + TWO_T;
constraint attack_II_time_complexity_4 = rf + 2 * tb + tf - BLOCK_BITS - 1 + 2 * sigma + TWO_T;
constraint attack_II_time_complexity_5 = sigma + ceiling_div(BLOCK_BITS, 2) + ceiling_div(TWO_T, 2);

var int: attack_II_time_complexity;
constraint maximum(attack_II_time_complexity, [
    attack_II_time_complexity_0,
    attack_II_time_complexity_1,
    attack_II_time_complexity_2,
    attack_II_time_complexity_3,
    attack_II_time_complexity_4,
    attack_II_time_complexity_5,
]);

var int: attack_III_time_complexity_0;
var int: attack_III_time_complexity_1;
constraint attack_III_time_complexity_0 = sigma + ceiling_div(BLOCK_BITS, 2) + ceiling_div(TWO_T, 2) + rb;
constraint attack_III_time_complexity_1 = rb - BLOCK_BITS + 2 * rf + TWO_T;

var int: attack_III_time_complexity;
constraint maximum(attack_III_time_complexity, [
    attack_III_time_complexity_0,
    attack_III_time_complexity_1,
]);

% constraint ATTACK_I = (
%     ((rf < BLOCK_BITS) /\ (rb < BLOCK_BITS)) /\
%     (2 + BLOCK_BITS + TWO_T < 2 * min(KEY_BITS - key_proba, BLOCK_BITS)) /\
%     (TWO_T + 2 * rb + 2 * rf < 2 * (KEY_BITS - key_proba) - BLOCK_BITS - 2)
% );

% constraint ATTACK_II = (
%     ((rf < BLOCK_BITS) /\ (rb < BLOCK_BITS)) /\
%     (2 + BLOCK_BITS + TWO_T < 2 * min(KEY_BITS - key_proba, BLOCK_BITS)) /\
%     (TWO_T + rf < KEY_BITS - key_proba - 1) /\
%     (TWO_T + tf < KEY_BITS - key_proba - 2) /\
%     (TWO_T + 2 * tf + 2 * rb < KEY_BITS - key_proba + BLOCK_BITS) /\
%     (TWO_T + rb + tb + 2 * tf < KEY_BITS - key_proba + BLOCK_BITS - 1) /\
%     (TWO_T + rf + 2 * tb + tf < KEY_BITS - key_proba + BLOCK_BITS - 1)
% );

% constraint ATTACK_III = (
%    ((rf < BLOCK_BITS) /\ (rb < BLOCK_BITS)) /\
%    (2 + BLOCK_BITS + TWO_T < 2 * min(KEY_BITS - key_proba, BLOCK_BITS)) /\
%    (2 * rb + 2 + BLOCK_BITS + TWO_T < 2 * (KEY_BITS - key_proba)) /\
%    (rb - BLOCK_BITS + 2 * rf + TWO_T < (KEY_BITS - key_proba))
% );

% constraint (ATTACK_I + ATTACK_II + ATTACK_III) > 0;

constraint forall(i in MAIN_ROUNDS, j in ROWS, k in BLOCK_COLS) (
    freeZupper[i, j, k] = (freeYupper(i, 0, k) \/ freeYupper(i, 1, k) \/ freeYupper(i, 2, k) \/ freeYupper(i, 3, k))
);

constraint forall(i in MAIN_ROUNDS, k in BLOCK_COLS) (
    sum (j in ROWS) (DYupper(i, j, k)) + sum(j in ROWS) (DZupper[i, j, k]) in {0, 5, 6, 7, 8}
);

constraint forall(i1 in range(1, Nr - 1), k1 in BLOCK_COLS, j in ROWS, i2 in range(1, Nr - 1), k2 in BLOCK_COLS where isGreater(i2, k2, i1, k1)) (
    XOR(diffYupper[i1, k1, j, i2, k2], DYupper(i1, j, k1), DYupper(i2, j, k2)) /\
    diffYupper[i1, k1, j, i2, k2] == diffYupper[i2, k2, j, i1, k1] /\
    XOR(diffZupper[i1, k1, j, i2, k2], DZupper[i1, j, k1], DZupper[i2, j, k2]) /\
    diffZupper[i1, k1, j, i2, k2] == diffZupper[i2, k2, j, i1, k1] /\
    forall (i3 in MAIN_ROUNDS, k3 in BLOCK_COLS where isGreater(i3, k3, i2, k2)) (
      diffYupper[i1, k1, j, i2, k2] + diffYupper[i2, k2, j, i3, k3] + diffYupper[i1, k1, j, i3, k3] != 1 /\
      diffZupper[i1, k1, j, i2, k2] + diffZupper[i2, k2, j, i3, k3] + diffZupper[i1, k1, j, i3, k3] != 1
    )
);

constraint forall(i1 in MAIN_ROUNDS, k1 in BLOCK_COLS, i2 in MAIN_ROUNDS, k2 in BLOCK_COLS where isGreater(i2, k2, i1, k1)) (
    sum (j in ROWS) (diffZupper[i1, k1, j, i2, k2]) + sum (j in ROWS) (diffYupper[i1, k1, j, i2, k2]) in {0, 5, 6, 7, 8}
);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Lower trail
array [ROUNDS, ROWS, BLOCK_COLS] of var bool: DXlower;
array [ROUNDS, ROWS, BLOCK_COLS] of var bool: freeXlower;

function var bool: DSBlower(int: i, int: j, int: k) = DXlower[i, j, k];
array [ROUNDS, ROWS, BLOCK_COLS] of var bool: freeSBlower;

function var bool: DYlower(int: i, int: j, int: k) = DSBlower(i, j, (k + SHIFT_ROWS[SHIFT_ROW_INDEX, j]) mod Nb);
function var bool: freeYlower(int: i, int: j, int: k) = freeSBlower[i, j, (k + SHIFT_ROWS[SHIFT_ROW_INDEX, j]) mod Nb];
array [MAIN_ROUNDS, BLOCK_COLS, ROWS, MAIN_ROUNDS, BLOCK_COLS] of var bool: diffYlower;

array [MAIN_ROUNDS, ROWS, BLOCK_COLS] of var bool: DZlower;
array [MAIN_ROUNDS, ROWS, BLOCK_COLS] of var bool: freeZlower;
array [MAIN_ROUNDS, BLOCK_COLS, ROWS, MAIN_ROUNDS, BLOCK_COLS] of var bool: diffZlower;

constraint forall(i in MAIN_ROUNDS, j in ROWS, k in BLOCK_COLS) (
    freeYlower(i, j, k) = (freeZlower[i, 0, k] \/ freeZlower[i, 1, k] \/ freeZlower[i, 2, k] \/ freeZlower[i, 3, k])
);

constraint forall(i in MAIN_ROUNDS, k in BLOCK_COLS) (
    sum (j in ROWS) (DYlower(i, j, k)) + sum(j in ROWS) (DZlower[i, j, k]) in {0, 5, 6, 7, 8}
);

constraint forall(i1 in range(1, Nr - 1), k1 in BLOCK_COLS, j in ROWS, i2 in range(1, Nr - 1), k2 in BLOCK_COLS where isGreater(i2, k2, i1, k1)) (
    XOR(diffYlower[i1, k1, j, i2, k2], DYlower(i1, j, k1), DYlower(i2, j, k2)) /\
    diffYlower[i1, k1, j, i2, k2] == diffYlower[i2, k2, j, i1, k1] /\
    XOR(diffZlower[i1, k1, j, i2, k2], DZlower[i1, j, k1], DZlower[i2, j, k2]) /\
    diffZlower[i1, k1, j, i2, k2] == diffZlower[i2, k2, j, i1, k1] /\
    forall (i3 in MAIN_ROUNDS, k3 in BLOCK_COLS where isGreater(i3, k3, i2, k2)) (
      diffYlower[i1, k1, j, i2, k2] + diffYlower[i2, k2, j, i3, k3] + diffYlower[i1, k1, j, i3, k3] != 1 /\
      diffZlower[i1, k1, j, i2, k2] + diffZlower[i2, k2, j, i3, k3] + diffZlower[i1, k1, j, i3, k3] != 1
    )
);

constraint forall(i1 in MAIN_ROUNDS, k1 in BLOCK_COLS, i2 in MAIN_ROUNDS, k2 in BLOCK_COLS where isGreater(i2, k2, i1, k1)) (
    sum (j in ROWS) (diffZlower[i1, k1, j, i2, k2]) + sum (j in ROWS) (diffYlower[i1, k1, j, i2, k2]) in {0, 5, 6, 7, 8}
);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

constraint forall(i in ROUNDS, j in ROWS, k in BLOCK_COLS) (
  one_objective_constraint(
    DXupper[i, j, k], freeXupper[i, j, k], freeSBupper[i, j, k],
    DXlower[i, j, k], freeXlower[i, j, k], freeSBlower[i, j, k]
  )
);

predicate one_objective_constraint(
    var bool: DXupper, var bool: freeXupper, var bool: freeSBupper, 
    var bool: DXlower, var bool: freeXlower, var bool: freeSBlower
) = (
    (freeSBupper -> DXupper) /\
    (freeXlower -> DXlower) /\
    (freeXupper -> freeSBupper) /\
    (freeSBlower -> freeXlower) /\
    ( freeXupper + freeSBupper + freeXlower + freeSBlower <= 2 )
    % Same version with Quine-McCluskey
    % (not(freeXupper) /\ not(freeSBupper) /\ not(freeXlower) /\ not(freeSBlower)) \/
    % (not(freeXupper) /\ not(freeSBupper) /\ DXlower /\ freeXlower) \/
    % (DXupper /\ not(freeXupper) /\ DXlower /\ not(freeSBlower)) \/
    % (DXupper /\ freeSBupper /\ not(freeXlower) /\ not(freeSBlower))
);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

predicate XOR(var bool: A, var bool: B, var bool: C) = A + B + C != 1;
predicate isGreater(int: i1, int: k1, int: i2, int: k2) = (i1 > i2) \/ (i1 == i2 /\ k1 > k2);
predicate isGte(int: i1, int: k1, int: i2, int: k2) = (i1 > i2) \/ (i1 == i2 /\ k1 >= k2);

predicate maximum(var int: x, array[int] of var int: arr) = forall(el in arr) (x >= el) /\ exists(el in arr) (x == el);
predicate minimum(var int: x, array[int] of var int: arr) = forall(el in arr) (x <= el) /\ exists(el in arr) (x == el);
function var int: ceiling_div(var int: x, var int: y) = (x div y) + (x mod y != 0);