C'est quoi un guess ?

Nombre de tour avant et après ($N_b$ et $N_f$)

$t_f$ = $t_b$ = nb octets actifs $\times$ $2^7$

B compte en haut, F compte en bas

## Notes

### Ajout de la complexité des attaques

#### Ajout des contraintes de validation des attaques

#### Ajout de l'optimalité

**Idée :** chaque complexité d'attaque est convertie en mesure calculable lors de la Step 1. Les complexités d'attaque s'expriment en somme de puissance de deux. Nous estimons que les termes de puissance inférieur (ex. $2^{17}$ vs $2^{18}$) sont négligables, ainsi pour chaque attaque nous avons :

$$
\mathtt{time\_complexity}_A = \max_{SA \in \mathtt{attack\_steps}}(\mathtt{time\_complexity}_{SA})
$$

Comme nous recherchons l'attaque la plus efficace, nous considérons que la complexité de l'attaque est égale au minimum des complexités des différentes attaques :

$$
\mathtt{time\_complexity} = \min_{A \in \mathtt{attacks}}(\mathtt{time\_complexity}_A)
$$

Formules :

$$
\mathtt{s} = 2^{2\sigma} \\
\mathtt{p2q2r} = 2^{-2t} \\
Y = 2^{\sigma + n/2 - rb + t} \\
\epsilon = 2^{-2\sigma} \\
~ \\
Attack I \\
Data Complexity \\
2^{2 + \sigma + n/2 + t} \\
Time Complexity \\
2^{rf + 2 + \sigma + n/2 + t + rb} \\
Memory Complexity \\
2^{2 + \sigma + n/2 + t} + 2^{rb + rf} \\
~ \\
Attack II \\
Data Complexity \\
2^{\sigma + n/2 + t} \\
Time Complexity \\
2^{rf - 1 + 2\sigma + 2t} + 2^{tf + 2\sigma + 2t} + 2^{2tf + 2rb - n - 2 + 2\sigma + 2t} + 2^{rb + tb + 2tf - n - 1 + 2\sigma + 2t} + 2^{rf + 2tb + tf - n - 1 + 2\sigma + 2t} + 2^{\sigma + n/2 + t} \\
~ \\
Attack III \\
Data Complexity \\
2^{2 + \sigma + n/2 + t} \\
Time Complexity \\
2^{\sigma + n/2 + t + rb} + 2^{rb - n + 2rf + 2t} \\
$$
