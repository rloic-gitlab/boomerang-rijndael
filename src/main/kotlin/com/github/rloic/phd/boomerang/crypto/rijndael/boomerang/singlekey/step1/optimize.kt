package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.singlekey.step1


import com.github.rloic.phd.boomerang.crypto.rijndael.Mod
import com.github.rloic.phd.boomerang.crypto.rijndael.RijndaelModels
import com.github.rloic.phd.boomerang.crypto.rijndael.Tmp
import com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.parsers.OptimizeSkStep1SolutionParser
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.SkRijndael
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.presenters.SkStep1SolutionLatexPresenter
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.OptimizeRijndaelBoomerangSkStep1Solution
import com.github.rloic.phd.core.error.panic
import com.github.rloic.phd.core.files.latex.Latex
import com.github.rloic.phd.core.mzn.*
import com.github.rloic.phd.core.utils.parseArgs
import com.github.rloic.phd.infra.MiniZincBinary
import com.github.rloic.phd.infra.OrToolsBinary
import com.github.rloic.phd.infra.PicatBinary
import javaextensions.io.div
import javaextensions.io.mkdirs

fun main(_args_: Array<String>) {
    val args = parseArgs(_args_)

    val minizinc = MiniZincBinary.from(args)
    val picat = PicatBinary.from(args, minizinc)
    val configuration = SkRijndael.from(args)

    val solution = minimizeSk(picat, configuration)
    if (solution != null) {
        Latex.preview(::SkStep1SolutionLatexPresenter, solution)
        println(solution.objStep1)
    } else {
        println("UNSAT")
    }
}

fun minimizeSk(mznSolver: MznSolver, config: SkRijndael): OptimizeRijndaelBoomerangSkStep1Solution? {
    val tmpFolder = mkdirs(Tmp / Mod.SingleKey / config.textSize.nbBits) ?: panic("Cannot create temp folder")

    val mznModel = MznModelBuilder.Optimization(
        listOf(
            PartialMznModel(RijndaelModels / "Main.mzn"),
            PartialMznModel(RijndaelModels / "${Mod.SingleKey}.mzn")
        ), MznVariable("objective"), OptimizationSearch.Minimize
    ).build(tmpFolder / "step1_minimize_${config.Nr}.mzn")

    val mznSolution = mznSolver.optimize(
        mznModel, mapOf(
            "Nr" to config.Nr,
            "BLOCK_BITS" to config.textSize.nbBits
        )
    )

    return mznSolution?.let(OptimizeSkStep1SolutionParser(config)::parse)
}