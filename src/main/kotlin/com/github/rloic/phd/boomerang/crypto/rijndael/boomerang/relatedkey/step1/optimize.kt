package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step1

import com.github.rloic.phd.boomerang.crypto.rijndael.Mod
import com.github.rloic.phd.boomerang.crypto.rijndael.RijndaelModels
import com.github.rloic.phd.boomerang.crypto.rijndael.Tmp
import com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.parsers.OptimizeRkStep1SolutionParser
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.RkRijndael
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.OptimizeRijndaelBoomerangRkStep1Solution
import com.github.rloic.phd.core.error.panic
import com.github.rloic.phd.core.mzn.*
import com.github.rloic.phd.core.utils.Logger
import com.github.rloic.phd.core.utils.logger
import com.github.rloic.phd.core.utils.parseArgs
import com.github.rloic.phd.infra.MiniZincBinary
import com.github.rloic.phd.infra.PicatBinary
import comb.github.rloic.boomerang.sat.rijndael.utils.KeySchedule
import javaextensions.io.div
import javaextensions.io.mkdirs
import kotlin.math.log

data class Record(val objStep1: Int, val textProba: Int, val keyProba: Int)

fun main(_args_: Array<String>) {
    val args = parseArgs(_args_)
    logger = Logger.from(args)
    logger.addTerminal()

    val minizinc = MiniZincBinary.from(args)
    val picat = PicatBinary.from(args, minizinc)
    val configuration = RkRijndael.from(args)

    var solution = optimizeRkKeyFirst(picat, configuration)
    var nbSol = 0

    var record: Record? = null

    while (solution != null) {
        // Latex.preview(::RkStep1SolutionLatexPresenter, solution)
        logger.log("Solution: %d", ++nbSol)
        logger.log("OjbStep1: %d", solution.objStep1)
        logger.log("TextProba: 2^{-%d}", solution.twoT)
        logger.log("KeyProba: 2^{-%d}", solution.keyProba)
        logger.log("rb: %d", solution.rb)
        logger.log("rf: %d", solution.rf)
        logger.log("---")

        record = Record(solution.objStep1, solution.twoT, solution.keyProba)
        if (record.objStep1 == 0) break

        solution = optimizeRkKeyFirst(picat, configuration, solution.keyProba to solution.twoT)
    }

    logger.log("ObjStep1, TextProba, KeyProba")
    if (record != null) {
        logger.log("2^{-%d}, 2^{-%d}, 2^{-%d}", record.objStep1, record.textProba, record.keyProba)
    } else {
        logger.log("NaN, Nan, Nan")
    }

}

fun optimizeRk(mznSolver: MznSolver, config: RkRijndael, ub: Int? = null): OptimizeRijndaelBoomerangRkStep1Solution? {
    val tmpFolder = mkdirs(Tmp / Mod.RelatedKey / config.textSize.nbBits / config.keySize.nbBits)
        ?: panic("Cannot create temp folder")

    val mznModel = MznModelBuilder.Optimization(
        listOf(
            PartialMznModel(RijndaelModels / "Main.mzn"),
            PartialMznModel(RijndaelModels / "${Mod.RelatedKey}.mzn")
        ), MznVariable("time_complexity"), OptimizationSearch.Minimize
    ).build(tmpFolder / "step1_minimize_${config.Nr}.mzn")

    if (ub != null) {
        mznModel.value.appendText("constraint objective >= $ub;\n")
    }

    KeySchedule.generate(config, "upper", mznModel)
    KeySchedule.generate(config, "lower", mznModel)

    val mznSolution = mznSolver.optimize(
        mznModel, mapOf(
            "Nr" to config.Nr,
            "BLOCK_BITS" to config.textSize.nbBits,
            "KEY_BITS" to config.keySize.nbBits
        )
    )

    return mznSolution?.let(OptimizeRkStep1SolutionParser(config)::parse)
}

fun optimizeRkKeyFirst(mznSolver: MznSolver, config: RkRijndael, probas: Pair<Int, Int>? = null): OptimizeRijndaelBoomerangRkStep1Solution? {
    val tmpFolder = mkdirs(Tmp / Mod.RelatedKey / config.textSize.nbBits / config.keySize.nbBits)
        ?: panic("Cannot create temp folder")

    val mznModel = MznModelBuilder.Optimization(
        listOf(
            PartialMznModel(RijndaelModels / "Main.mzn"),
            PartialMznModel(RijndaelModels / "${Mod.RelatedKey}.mzn")
        ), MznVariable("time_complexity"), OptimizationSearch.Minimize
    ).build(tmpFolder / "step1_minimize_${config.Nr}.mzn")

    mznModel.value.writeText(
        mznModel.value.readText().replace("solve minimize objective;", "")
    )

    if (probas != null) {
        val (keyProba, textProba) = probas
        mznModel.value.appendText("constraint (key_proba < $keyProba) \\/ (key_proba == $keyProba /\\ TWO_T < $textProba );\n")
    }

    KeySchedule.generate(config, "upper", mznModel)
    KeySchedule.generate(config, "lower", mznModel)

    val mznSolution = mznSolver.optimize(
        mznModel, mapOf(
            "Nr" to config.Nr,
            "BLOCK_BITS" to config.textSize.nbBits,
            "KEY_BITS" to config.keySize.nbBits
        )
    )

    return mznSolution?.let(OptimizeRkStep1SolutionParser(config)::parse)
}