package comb.github.rloic.boomerang.sat.skinny

import com.github.rloic.phd.boomerang.crypto.rijndael.Tmp
import com.github.rloic.phd.boomerang.crypto.skinny.Mod
import com.github.rloic.phd.core.error.panic
import com.github.rloic.phd.core.mzn.*
import com.github.rloic.phd.core.utils.FromArgs
import com.github.rloic.phd.core.utils.expectArgument
import com.github.rloic.phd.core.utils.parseArgs
import com.github.rloic.phd.infra.MiniZincBinary
import com.github.rloic.phd.infra.PicatBinary
import javaextensions.io.div
import javaextensions.io.mkdirs
import java.io.File

val CURRENT_FOLDER = File("sat/skinny")
val TMP = CURRENT_FOLDER / "tmp"

data class Config(val Nr: Int, val mod: Mod) {
    companion object : FromArgs<Config> {
        override fun from(args: Map<String, String>) = Config(
            args.expectArgument("Nr").toInt(),
            Mod.valueOf(args.expectArgument("Mod"))
        )
    }
}

fun main(_args_: Array<String>) {
    val arguments = parseArgs(_args_)

    val config = Config.from(arguments)
    val minizinc = MiniZincBinary.from(arguments)
    val picat = PicatBinary.from(arguments, minizinc)

    // val printTexFile = arguments["PrintTex"]?.toBoolean() ?: false

    println("sat/skinny/MinimizeSep1 Nr=${config.Nr} mod=${config.mod}")

    println(
        minimizeSk(picat, config)
    )
}



fun minimizeSk(mznSolver: MznSolver, config: Config): MznSolution? {
    val tmpFolder = mkdirs(File("sat/skinny/tmp") / config.mod) ?: panic()

    val mznModel = MznModelBuilder.Optimization(
        listOf(
            PartialMznModel(File("sat/skinny/main.mzn")),
            PartialMznModel(File("sat/skinny/linear_SK.mzn"))
        ), MznVariable("objective"), OptimizationSearch.Minimize
    ).build(tmpFolder / "minimize_step1_${config.Nr}.mzn")

    val mznSolution = mznSolver.optimize(mznModel, mapOf(
        "R" to config.Nr
    ))

    return mznSolution

}

fun File.append(other: File) = appendText(other.readText()).apply { appendText("\n\n") }
