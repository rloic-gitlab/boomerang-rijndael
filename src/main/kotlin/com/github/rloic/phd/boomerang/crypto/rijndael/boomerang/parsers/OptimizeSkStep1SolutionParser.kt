package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.parsers

import com.github.rloic.phd.core.cryptography.ciphers.rijndael.SkRijndael
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.OptimizeRijndaelBoomerangSkStep1Solution
import com.github.rloic.phd.core.mzn.MznSolution
import com.github.rloic.phd.core.mzn.MznSolutionParser

@Suppress("NonAsciiCharacters")
class OptimizeSkStep1SolutionParser(val config: SkRijndael) : MznSolutionParser<OptimizeRijndaelBoomerangSkStep1Solution> {
    private val delegate = EnumerateSkStep1SolutionParser(config)
    override fun parse(serialized: MznSolution): OptimizeRijndaelBoomerangSkStep1Solution {
        val elements = serialized.content.split('\n')
        val objStep1 = elements.getIntValue("objective")
        val solution = delegate.parse(serialized)
        return OptimizeRijndaelBoomerangSkStep1Solution(solution.config, objStep1, solution.X, solution.Y, solution.Z, solution.ATTACK_I, solution.ATTACK_II, solution.ATTACK_III, solution.keyProba, solution.twoT, solution.rb, solution.rf, solution.timeComplexity)
    }
}