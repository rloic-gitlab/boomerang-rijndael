package com.github.rloic.phd.boomerang.crypto.rijndael

import javaextensions.io.div
import java.io.File

val RijndaelModels = File("sat/rijndael")
val Tmp = RijndaelModels / "tmp"
val Figures = File("figures")