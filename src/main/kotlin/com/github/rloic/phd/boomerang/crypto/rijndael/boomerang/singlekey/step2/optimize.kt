package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.singlekey.step2

import com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.singlekey.step1.enumerateSk
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.SkRijndael
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.presenters.SkStep2SolutionLatexPresenter
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.OptimizeSkStep2Solution
import com.github.rloic.phd.core.io.Console.INFO
import com.github.rloic.phd.core.utils.expectArgument
import com.github.rloic.phd.core.utils.parseArgs
import com.github.rloic.phd.infra.MiniZincBinary
import com.github.rloic.phd.infra.PicatBinary
import java.io.File
import kotlin.math.absoluteValue


fun main(_args_: Array<String>) {
    val args = parseArgs(_args_)

    val minizinc = MiniZincBinary.from(args)
    val picat = PicatBinary.from(args, minizinc)
    val step1Config = SkRijndael.from(args)
    var upperBound = -args.expectArgument("ObjStep1").toInt()
    val limit = args["Limit"]?.toInt()

    val step2Output = args["Step2OutFile"]
    var iterator = enumerateSk(picat, step1Config, upperBound.absoluteValue)

    var lowerBound: Int? = null
    fun lowerBoundValue() = lowerBound ?: -Int.MAX_VALUE

    var counter = 0

    while (lowerBoundValue() < upperBound) {
        println("$INFO $step1Config UpperBound: 2^{$upperBound} LowerBound: 2^{${lowerBoundValue()}}")
        if (iterator.hasNext() && (limit == null || counter < limit)) {
            counter += 1
            val step1Sol = iterator.next()
            val step2Sol = RijndaelSKStep2(step1Sol, lowerBoundValue().absoluteValue)

            val step2Solver = step2Sol.model.solver
            while (step2Solver.solve()) {
                counter = 0
                lowerBound = -step2Sol.objective.value

                if (step2Output != null) {
                    val step2Solution = step2Sol.into()
                    val outFile = File(
                        step2Output
                            .replace("{Nr}", step1Config.Nr.toString())
                            .replace("{TextSize}", step1Config.textSize.nbBits.toString())
                            .replace("{ObjStep2}", step2Solution.objStep2.toString())
                    )
                    outFile.parentFile?.mkdirs()
                    val solutionWriter = SkStep2SolutionLatexPresenter<OptimizeSkStep2Solution>(outFile.bufferedWriter())
                    solutionWriter.present(step2Solution)
                    solutionWriter.close()
                }

                println("$INFO New best probability: 2^{$lowerBound}")
            }
        } else {
            println("$INFO No more solution")
            upperBound -= 1
            counter = 0
            iterator = enumerateSk(picat, step1Config, upperBound.absoluteValue)
        }
    }
    println("$INFO $step1Config UpperBound: 2^{$upperBound} LowerBound: 2^{${lowerBoundValue()}}")

    if (lowerBound != null) {
        println("Best probability")
        println("2^{$lowerBound}")
    } else {
        println("No solution can be found")
    }

}
