package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.parsers

import com.github.rloic.phd.core.arrays.Tensor3
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.SkRijndael
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.*
import com.github.rloic.phd.core.mzn.MznSolution
import com.github.rloic.phd.core.mzn.MznSolutionParser
import com.github.rloic.phd.core.utils.logger
import javaextensions.util.reshape

@Suppress("NonAsciiCharacters")
class EnumerateSkStep1SolutionParser(val config: SkRijndael) :
    MznSolutionParser<EnumerateRijndaelBoomerangSkStep1Solution> {
    override fun parse(serialized: MznSolution): EnumerateRijndaelBoomerangSkStep1Solution {
        val Nr = config.Nr
        val Nb = config.Nb

        val elements = serialized.content.split('\n')

        val ΔXupper = elements.getIntArray("DXupper").reshape(Nr, 4, Nb)
        val freeXupper = elements.getIntArray("freeXupper").reshape(Nr, 4, Nb)
        val freeSBupper = elements.getIntArray("freeSBupper").reshape(Nr, 4, Nb)

        val ΔXlower = elements.getIntArray("DXlower").reshape(Nr, 4, Nb)
        val freeXlower = elements.getIntArray("freeXlower").reshape(Nr, 4, Nb)
        val freeSBlower = elements.getIntArray("freeSBlower").reshape(Nr, 4, Nb)

        val X = Tensor3(Nr, 4, Nb) { i, j, k ->
            BoomerangSbVar(
                TrailVar(ΔXupper[i, j, k], ΔXlower[i, j, k]),
                TrailVar(freeXupper[i, j, k], freeXlower[i, j, k]),
                TrailVar(freeSBupper[i, j, k], freeSBlower[i, j, k]),
            )
        }

        val Y = Tensor3(X.dim1, X.dim2, X.dim3) { i, j, k ->
            val kx = (k + config.shift[j]) % Nb
            BoomerangLinVar(
                TrailVar(ΔXupper[i, j, kx], ΔXlower[i, j, kx]),
                TrailVar(freeSBupper[i, j, kx], freeSBlower[i, j, kx])
            )
        }

        val ΔZupper = elements.getIntArray("DZupper").reshape(Nr - 1, 4, Nb)
        val freeZupper = elements.getIntArray("freeZupper").reshape(Nr - 1, 4, Nb)

        val ΔZlower = elements.getIntArray("DZlower").reshape(Nr - 1, 4, Nb)
        val freeZlower = elements.getIntArray("freeZlower").reshape(Nr - 1, 4, Nb)

        val Z = Tensor3(Nr - 1, 4, Nb) { i, j, k ->
            BoomerangLinVar(
                TrailVar(ΔZupper[i, j, k], ΔZlower[i, j, k]),
                TrailVar(freeZupper[i, j, k], freeZlower[i, j, k])
            )
        }

        val timeComplexity = elements.getIntValueOr(
            "time_complexity",
            intArrayOf(
                elements.getIntValue("attack_I_time_complexity"),
                elements.getIntValue("attack_II_time_complexity"),
                elements.getIntValue("attack_III_time_complexity"),
            ).minOrNull()!!
        )
        logger.trace("Time complexity = %d", timeComplexity)
        logger.trace("attack_I_time_complexity = %d", elements.getIntValue("attack_I_time_complexity"))
        logger.trace("attack_II_time_complexity = %d", elements.getIntValue("attack_II_time_complexity"))
        logger.trace("attack_II_time_complexity_0 = %d", elements.getIntValue("attack_II_time_complexity_0"))
        logger.trace("attack_II_time_complexity_1 = %d", elements.getIntValue("attack_II_time_complexity_1"))
        logger.trace("attack_II_time_complexity_2 = %d", elements.getIntValue("attack_II_time_complexity_2"))
        logger.trace("attack_II_time_complexity_3 = %d", elements.getIntValue("attack_II_time_complexity_3"))
        logger.trace("attack_II_time_complexity_4 = %d", elements.getIntValue("attack_II_time_complexity_4"))
        logger.trace("attack_II_time_complexity_5 = %d", elements.getIntValue("attack_II_time_complexity_5"))
        logger.trace("attack_III_time_complexity = %d", elements.getIntValue("attack_III_time_complexity"))
        logger.trace("attack_III_time_complexity_0 = %d", elements.getIntValue("attack_III_time_complexity_0"))
        logger.trace("attack_III_time_complexity_1 = %d", elements.getIntValue("attack_III_time_complexity_1"))

        val ATTACK_I = true // elements.getBoolValue("ATTACK_I")
        logger.debug("ATTACK_I = %b", ATTACK_I)
        val ATTACK_II = true // elements.getBoolValue("ATTACK_II")
        logger.debug("ATTACK_II = %b", ATTACK_II)
        val ATTACK_III = true // elements.getBoolValue("ATTACK_III")
        logger.debug("ATTACK_III = %b", ATTACK_III)

        val keyProba = elements.getIntValue("key_proba")
        logger.debug("key_proba = %d", keyProba)
        val twoT = elements.getIntValue("TWO_T")
        logger.debug("2t = %d", twoT)

        val rb = elements.getIntValue("rb")
        logger.debug("rb = %d", rb)

        val rf = elements.getIntValue("rf")
        logger.debug("rf = %d", rf)

        return EnumerateRijndaelBoomerangSkStep1Solution(config, X, Y, Z, ATTACK_I, ATTACK_II, ATTACK_III, keyProba, twoT, rb, rf, timeComplexity)
    }
}