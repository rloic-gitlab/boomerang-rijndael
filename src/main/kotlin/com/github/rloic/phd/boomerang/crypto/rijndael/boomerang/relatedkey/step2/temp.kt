package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step2

import com.github.rloic.phd.boomerang.crypto.rijndael.Mod
import com.github.rloic.phd.boomerang.crypto.rijndael.RijndaelModels
import com.github.rloic.phd.boomerang.crypto.rijndael.Tmp
import com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.parsers.EnumerateRkStep1SolutionParser
import com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.parsers.OptimizeRkStep1SolutionParser
import com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step1.Record
import com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step1.optimizeRkKeyFirst
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.RkRijndael
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.presenters.RkStep2SolutionLatexPresenter
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.presenters.TextPresenter
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.OptimizeRijndaelBoomerangRkStep1Solution
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.OptimizeRkStep2Solution
import com.github.rloic.phd.core.error.panic
import com.github.rloic.phd.core.mzn.*
import com.github.rloic.phd.core.utils.*
import com.github.rloic.phd.core.utils.Logger.Level
import com.github.rloic.phd.infra.MiniZincBinary
import com.github.rloic.phd.infra.PicatBinary
import comb.github.rloic.boomerang.sat.rijndael.utils.KeySchedule
import javaextensions.io.div
import javaextensions.io.mkdirs
import org.chocosolver.solver.objective.ParetoOptimizer
import java.io.File
import java.time.Duration
import java.time.Instant
import kotlin.math.absoluteValue

fun optimizeRk(mznSolver: MznSolver, config: RkRijndael, ub: Int? = null): MznSolution? {
    val tmpFolder = mkdirs(Tmp / Mod.RelatedKey / config.textSize.nbBits / config.keySize.nbBits)
        ?: panic("Cannot create temp folder")

    val mznModel = MznModelBuilder.Optimization(
        listOf(
            PartialMznModel(RijndaelModels / "Main.mzn"),
            PartialMznModel(RijndaelModels / "${Mod.RelatedKey}.mzn")
        ), MznVariable("time_complexity"), OptimizationSearch.Minimize
    ).build(tmpFolder / "step1_minimize_${config.Nr}.mzn")

    if (ub != null) {
        mznModel.value.appendText("constraint objective >= $ub;\n")
    }

    KeySchedule.generate(config, "upper", mznModel)
    KeySchedule.generate(config, "lower", mznModel)

    val mznSolution = mznSolver.optimize(
        mznModel, mapOf(
            "Nr" to config.Nr,
            "BLOCK_BITS" to config.textSize.nbBits,
            "KEY_BITS" to config.keySize.nbBits
        )
    )

    return mznSolution
}

fun main(_args_: Array<String>) {
    val args = parseArgs(_args_)
    val minizinc = MiniZincBinary.from(args)
    val picat = PicatBinary.from(args, minizinc)
    val step1Config = RkRijndael.from(args)

    logger = Logger.from(args)
    logger.addTerminal()

    val step2Output = args["Step2OutFile"]

    var step1OptTime = Duration.ZERO
    var step1EnumTime = Duration.ZERO
    var step2OptTime = Duration.ZERO

    val startStep1Opt = Instant.now()

    val config = RkRijndael.from(args)
    val step1Sol = EnumerateRkStep1SolutionParser(config).parse(optimizeRk(picat, config)!!)
    logger.log("Time to compute Step1Opt = %.2fs", startStep1Opt.elapsed().toMillis() / 1000.0)

    var lowerBound = -128
    val step2Sol = RijndaelRKStep2(step1Sol, lowerBound.absoluteValue)

    val step2Solver = step2Sol.model.solver
    while (step2Solver.solve()) {
        lowerBound = -step2Sol.objective.value

        step2Sol.model.or(
            step2Sol.model.arithm(step2Sol.keyProba, "<", step2Sol.keyProba.value),
            step2Sol.model.and(
                step2Sol.model.arithm(step2Sol.keyProba, "=", step2Sol.keyProba.value),
                step2Sol.model.arithm(step2Sol.twoT, "<", step2Sol.twoT.value)
            )
        ).post()

        logger.log("TextProba: 2^{-%d}", step2Sol.twoT.value)
        logger.log("KeyProba: 2^{-%d}", step2Sol.keyProba.value)
        logger.log("rb: %d", step2Sol.rb)
        logger.log("rf: %d", step2Sol.rf)
        logger.log("---")

        if (step2Output != null) {
            val step2Solution = step2Sol.into()
            val outFile = File(
                step2Output
                    .replace("{Nr}", step1Config.Nr.toString())
                    .replace("{TextSize}", step1Config.textSize.nbBits.toString())
                    .replace("{KeySize}", step1Config.keySize.nbBits.toString())
                    .replace("{ObjStep2}", step2Solution.objStep2.toString())
                    .replace("{TextProba}", step2Sol.twoT.value.toString())
                    .replace("{KeyProba}", step2Sol.keyProba.value.toString())
            )
            outFile.parentFile?.mkdirs()
            val solutionWriter =
                RkStep2SolutionLatexPresenter<OptimizeRkStep2Solution>(outFile.bufferedWriter())

            solutionWriter.present(step2Solution)
            solutionWriter.close()
        }            /* TextPresenter<OptimizeRkStep2Solution>("/home/rloic/Personnel/Code/CLion/compute-proba/solution/${outFile.nameWithoutExtension}").present(
                step2Solution
            ) */
        step2Solver.printShortStatistics()
        logger.info("New best probability: 2^{$lowerBound}")
    }

    logger.log(
        "%.2f,%.2f,%.2f,%s".format(
            step1OptTime.toMillis() / 1000.0,
            step1EnumTime.toMillis() / 1000.0,
            step2OptTime.toMillis() / 1000.0,
            lowerBound.absoluteValue
        )
    )
}