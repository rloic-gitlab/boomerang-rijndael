package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.singlekey.step1

import com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.parsers.EnumerateSkStep1SolutionParser
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.SkRijndael
import com.github.rloic.phd.core.mzn.Assignment
import com.github.rloic.phd.core.mzn.MznSolution
import javaextensions.util.mapToBool
import javaextensions.util.mapToIntArray

@Suppress("NonAsciiCharacters")
class RijndaelSkPartialAssignment(private val config: SkRijndael) {

    fun extractSolution(mznSolution: MznSolution): Assignment.Partial {
        val solution = EnumerateSkStep1SolutionParser(config).parse(mznSolution)

        // Upper trail
        val flattenedΔXupper = solution.X.deepFlatten().mapToIntArray { it.Δ.upper }.mapToBool().contentToString()
        val flattenedFreeXupper = solution.X.deepFlatten().mapToIntArray { it.free.upper }.mapToBool().contentToString()
        val flattenedFreeSBupper = solution.X.deepFlatten().mapToIntArray { it.freeS.upper }.mapToBool().contentToString()
        // Lower trail
        val flattenedΔXlower = solution.X.deepFlatten().mapToIntArray { it.Δ.lower }.mapToBool().contentToString()
        val flattenedFreeXlower = solution.X.deepFlatten().mapToIntArray { it.free.lower }.mapToBool().contentToString()
        val flattenedFreeSBlower = solution.X.deepFlatten().mapToIntArray { it.freeS.lower }.mapToBool().contentToString()

        return Assignment.Partial(buildString {
            // Upper trail
            appendLine("array [ROUNDS, ROWS, BLOCK_COLS] of var bool: DXupper_%SOL% = array3d(ROUNDS, ROWS, BLOCK_COLS, $flattenedΔXupper);")
            appendLine("array [ROUNDS, ROWS, BLOCK_COLS] of var bool: freeXupper_%SOL% = array3d(ROUNDS, ROWS, BLOCK_COLS, $flattenedFreeXupper);")
            appendLine("array [ROUNDS, ROWS, BLOCK_COLS] of var bool: freeSBupper_%SOL% = array3d(ROUNDS, ROWS, BLOCK_COLS, $flattenedFreeSBupper);")
            // Lower trail
            appendLine("array [ROUNDS, ROWS, BLOCK_COLS] of var bool: DXlower_%SOL% = array3d(ROUNDS, ROWS, BLOCK_COLS, $flattenedΔXlower);")
            appendLine("array [ROUNDS, ROWS, BLOCK_COLS] of var bool: freeXlower_%SOL% = array3d(ROUNDS, ROWS, BLOCK_COLS, $flattenedFreeXlower);")
            appendLine("array [ROUNDS, ROWS, BLOCK_COLS] of var bool: freeSBlower_%SOL% = array3d(ROUNDS, ROWS, BLOCK_COLS, $flattenedFreeSBlower);")
        })
    }

}