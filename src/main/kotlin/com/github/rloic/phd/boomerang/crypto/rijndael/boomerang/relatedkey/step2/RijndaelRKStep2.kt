package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step2

import com.github.rloic.phd.boomerang.utils.chunks
import com.github.rloic.phd.boomerang.utils.subsets
import com.github.rloic.phd.core.arrays.*
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isBCT
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isDDT2Lower
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isDDT2Upper
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isDDTLower
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isDDTupper
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isEBCT
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isLBCT
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangRules.isUBCT
import com.github.rloic.phd.core.cryptography.attacks.boomerang.BoomerangTable
import com.github.rloic.phd.core.cryptography.attacks.boomerang.util.Bounds
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.Rijndael.Companion.SBOX_TABLES
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.Rijndael.Companion.XOR_TUPLES
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.Rijndael.Companion.mul11
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.Rijndael.Companion.mul13
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.Rijndael.Companion.mul14
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.Rijndael.Companion.mul2
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.Rijndael.Companion.mul3
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.Rijndael.Companion.mul9
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.*
import com.github.rloic.phd.core.error.panic
import com.github.rloic.phd.core.utils.Into
import com.github.rloic.phd.core.utils.logger
import org.chocosolver.solver.IModel
import org.chocosolver.solver.Model
import org.chocosolver.solver.constraints.extension.Tuples
import org.chocosolver.solver.variables.IntVar
import org.chocosolver.solver.variables.Variable
import kotlin.math.min


@Suppress("NonAsciiCharacters")
class RijndaelRKStep2(
    private val step1: EnumerateRijndaelBoomerangRkStep1Solution,
    bestBound: Int,
    val model: Model = Model()
) : IModel by model, Into<OptimizeRkStep2Solution> {

    val FREE = model.intVar("FREE", 256)
    val ZERO = model.intVar("ZERO", 0)
    private val MEM = mutableMapOf<String, IntVar>()

    val Nr = step1.config.Nr
    val Nb = step1.config.Nb
    val Nk = step1.config.Nk

    val δXupper: Tensor3<IntVar>
    val δSXupper: Tensor3<IntVar>
    val δYupper: Tensor3<IntVar>
    val δZupper: Tensor3<IntVar>

    val δWKupper: Matrix<IntVar>
    val δSWKupper: Matrix<IntVar?>

    val δXlower: Tensor3<IntVar>
    val δSXlower: Tensor3<IntVar>
    val δYlower: Tensor3<IntVar>
    val δZlower: Tensor3<IntVar>

    val δWKlower: Matrix<IntVar>
    val δSWKlower: Matrix<IntVar?>

    val probas: Tensor3<IntVar?>
    val activeTable = Tensor3(Nr, 4, Nb) { _, _, _ -> BoomerangTable.None }

    val keyProbas: Matrix<IntVar?>
    val activeKeyTable = Matrix(4, Nb * (Nr + 1)) { _, _ -> BoomerangTable.None }

    val objective: IntVar
    val twoT: IntVar
    val keyProba: IntVar
    val timeComplexity: IntVar

    val rb: Int
    var rf: Int

    val ATTACK_ROUNDS = 1 until Nr - 1
    val ATTACK_KEY_COLS = 0 until Nb * Nr

    val LARGE_ALGO = "GAC3rm+"
    val BIN_ALGO = "AC3bit+rm"

    init {
        logger.info("Propagation algoritm for N-ary tables  = $LARGE_ALGO")
        logger.info("Propagation algoritm for Binary tables = $BIN_ALGO")
        δXupper = byteVars("δX↑", step1.X, TrailVar::upper, BoomerangSbVar::free)
        δSXupper = byteVars("δSX↑", step1.X, TrailVar::upper, BoomerangSbVar::freeS)

        δWKupper = byteVars("δWK↑", step1.WK, TrailVar::upper)
        δSWKupper = byteSBVars("δSWK↑", step1.WK, TrailVar::upper)

        δXlower = byteVars("δX↓", step1.X, TrailVar::lower, BoomerangSbVar::free)
        δSXlower = byteVars("δSX↓", step1.X, TrailVar::lower, BoomerangSbVar::freeS)

        δWKlower = byteVars("δWK↓", step1.WK, TrailVar::lower)
        δSWKlower = byteSBVars("δSWK↓", step1.WK, TrailVar::lower)

        probas = tensor3OfNulls(Nr, 4, Nb)
        keyProbas = matrixOfNulls(4, Nb * (Nr + 1))

        var objectiveBounds = Bounds(0, 0)
        var twoTBounds = Bounds(0, 0)
        var keyBounds = Bounds(0, 0)

        // SubCell
        for (i in 0 until Nr) {
            for (j in 0..3) {
                for (k in 0 until Nb) {
                    val ΔXupper = step1.X[i, j, k].Δ.upper
                    val freeXupper = step1.X[i, j, k].free.upper
                    val freeSBupper = step1.X[i, j, k].freeS.upper
                    val ΔXlower = step1.X[i, j, k].Δ.lower
                    val freeXlower = step1.X[i, j, k].free.lower
                    val freeSBlower = step1.X[i, j, k].freeS.lower

                    if (isDDTupper(ΔXupper, freeXupper, freeSBupper, ΔXlower, freeXlower, freeSBlower)) {
                        probas[i, j, k] = intVar(SBOX_TABLES.relationDDTBounds)
                        model.table(
                            arrayOf(δXupper[i, j, k], δSXupper[i, j, k], probas[i, j, k]!!),
                            SBOX_TABLES.relationDDT,
                            LARGE_ALGO
                        ).post()
                        if (i in ATTACK_ROUNDS) {
                            objectiveBounds += SBOX_TABLES.relationDDTBounds
                            twoTBounds += SBOX_TABLES.relationDDTBounds
                        }
                        activeTable[i, j, k] = BoomerangTable.DDT
                    }

                    if (isDDTLower(ΔXupper, freeXupper, freeSBupper, ΔXlower, freeXlower, freeSBlower)) {
                        probas[i, j, k] = intVar(SBOX_TABLES.relationDDTBounds)
                        model.table(
                            arrayOf(δXlower[i, j, k], δSXlower[i, j, k], probas[i, j, k]!!),
                            SBOX_TABLES.relationDDT,
                            LARGE_ALGO
                        ).post()
                        if (i in ATTACK_ROUNDS) {
                            objectiveBounds += SBOX_TABLES.relationDDTBounds
                            twoTBounds += SBOX_TABLES.relationDDTBounds
                        }
                        activeTable[i, j, k] = BoomerangTable.DDT
                    }

                    if (isDDT2Upper(
                            ΔXupper,
                            freeXupper,
                            freeSBupper,
                            ΔXlower,
                            freeXlower,
                            freeSBlower
                        )
                    ) {
                        probas[i, j, k] = intVar(SBOX_TABLES.relationDDT2Bounds)
                        model.table(
                            arrayOf(δXupper[i, j, k], δSXupper[i, j, k], probas[i, j, k]!!),
                            SBOX_TABLES.relationDDT2,
                            LARGE_ALGO
                        ).post()
                        if (i in ATTACK_ROUNDS) {
                            objectiveBounds += SBOX_TABLES.relationDDT2Bounds
                            twoTBounds += SBOX_TABLES.relationDDT2Bounds
                        }
                        activeTable[i, j, k] = BoomerangTable.DDT2
                    }

                    if (isDDT2Lower(
                            ΔXupper,
                            freeXupper,
                            freeSBupper,
                            ΔXlower,
                            freeXlower,
                            freeSBlower
                        )
                    ) {
                        probas[i, j, k] = intVar(SBOX_TABLES.relationDDT2Bounds)
                        model.table(
                            arrayOf(δXlower[i, j, k], δSXlower[i, j, k], probas[i, j, k]!!),
                            SBOX_TABLES.relationDDT2,
                            LARGE_ALGO
                        ).post()
                        if (i in ATTACK_ROUNDS) {
                            objectiveBounds += SBOX_TABLES.relationDDT2Bounds
                            twoTBounds += SBOX_TABLES.relationDDT2Bounds
                        }
                        activeTable[i, j, k] = BoomerangTable.DDT2
                    }

                    if (isBCT(ΔXupper, freeXupper, freeSBupper, ΔXlower, freeXlower, freeSBlower)) {
                        probas[i, j, k] = intVar(SBOX_TABLES.relationBCTBounds)
                        model.table(
                            arrayOf(δXupper[i, j, k], δSXlower[i, j, k], probas[i, j, k]!!),
                            SBOX_TABLES.relationBCT,
                            LARGE_ALGO
                        ).post()
                        if (i in ATTACK_ROUNDS) {
                            objectiveBounds += SBOX_TABLES.relationBCTBounds
                            twoTBounds += SBOX_TABLES.relationBCTBounds
                        }
                        activeTable[i, j, k] = BoomerangTable.BCT
                    }

                    if (isUBCT(ΔXupper, freeXupper, freeSBupper, ΔXlower, freeXlower, freeSBlower)) {
                        probas[i, j, k] = intVar(SBOX_TABLES.relationUBCTBounds)
                        model.table(
                            arrayOf(δXupper[i, j, k], δSXupper[i, j, k], δSXlower[i, j, k], probas[i, j, k]!!),
                            SBOX_TABLES.relationUBCT,
                            LARGE_ALGO
                        ).post()
                        if (i in ATTACK_ROUNDS) {
                            objectiveBounds += SBOX_TABLES.relationUBCTBounds
                            twoTBounds += SBOX_TABLES.relationUBCTBounds
                        }
                        activeTable[i, j, k] = BoomerangTable.UBCT
                    }

                    if (isLBCT(ΔXupper, freeXupper, freeSBupper, ΔXlower, freeXlower, freeSBlower)) {
                        probas[i, j, k] = intVar(SBOX_TABLES.relationLBCTBounds)
                        model.table(
                            arrayOf(δXupper[i, j, k], δXlower[i, j, k], δSXlower[i, j, k], probas[i, j, k]!!),
                            SBOX_TABLES.relationLBCT,
                            LARGE_ALGO
                        ).post()
                        if (i in ATTACK_ROUNDS) {
                            objectiveBounds += SBOX_TABLES.relationLBCTBounds
                            twoTBounds += SBOX_TABLES.relationLBCTBounds
                        }
                        activeTable[i, j, k] = BoomerangTable.LBCT
                    }

                    if (isEBCT(ΔXupper, freeXupper, freeSBupper, ΔXlower, freeXlower, freeSBlower)) {
                        probas[i, j, k] = intVar(SBOX_TABLES.relationEBCTBounds)
                        model.table(
                            arrayOf(
                                δXupper[i, j, k],
                                δSXupper[i, j, k],
                                δXlower[i, j, k],
                                δSXlower[i, j, k],
                                probas[i, j, k]!!
                            ),
                            SBOX_TABLES.relationEBCT,
                            LARGE_ALGO
                        ).post()
                        if (i in ATTACK_ROUNDS) {
                            objectiveBounds += SBOX_TABLES.relationEBCTBounds
                            twoTBounds += SBOX_TABLES.relationEBCTBounds
                        }
                        activeTable[i, j, k] = BoomerangTable.EBCT
                    }
                }
            }
        }

        // SubCell for KeySchedule
        for (j in 0..3) {
            for (k in 0 until Nb * (Nr + 1)) {
                if (step1.config.isSbColumn(k)) {
                    val ΔWKupper = step1.WK[j, k].Δ.upper
                    val freeWKupper = step1.WK[j, k].free.upper
                    val freeSWKupper = step1.WK[j, k].freeS!!.upper
                    val ΔWKlower = step1.WK[j, k].Δ.lower
                    val freeWKlower = step1.WK[j, k].free.lower
                    val freeSWKlower = step1.WK[j, k].freeS!!.lower

                    if (isDDTupper(ΔWKupper, freeWKupper, freeSWKupper, ΔWKlower, freeWKlower, freeSWKlower)) {
                        keyProbas[j, k] = intVar(SBOX_TABLES.relationDDTBounds)
                        model.table(
                            arrayOf(δWKupper[j, k], δSWKupper[j, k], keyProbas[j, k]!!),
                            SBOX_TABLES.relationDDT,
                            LARGE_ALGO
                        ).post()
                        if (k in ATTACK_KEY_COLS) {
                            objectiveBounds += SBOX_TABLES.relationDDTBounds
                            keyBounds += SBOX_TABLES.relationDDTBounds
                        }
                        activeKeyTable[j, k] = BoomerangTable.DDT
                    }

                    if (isDDTLower(ΔWKupper, freeWKupper, freeSWKupper, ΔWKlower, freeWKlower, freeSWKlower)) {
                        keyProbas[j, k] = intVar(SBOX_TABLES.relationDDTBounds)
                        model.table(
                            arrayOf(δWKlower[j, k], δSWKlower[j, k], keyProbas[j, k]!!),
                            SBOX_TABLES.relationDDT,
                            LARGE_ALGO
                        ).post()
                        if (k in ATTACK_KEY_COLS) {
                            objectiveBounds += SBOX_TABLES.relationDDTBounds
                            keyBounds += SBOX_TABLES.relationDDTBounds
                        }
                        activeKeyTable[j, k] = BoomerangTable.DDT
                    }

                    if (isDDT2Upper(ΔWKupper, freeWKupper, freeSWKupper, ΔWKlower, freeWKlower, freeSWKlower)) {
                        keyProbas[j, k] = intVar(SBOX_TABLES.relationDDT2Bounds)
                        model.table(
                            arrayOf(δWKupper[j, k], δSWKupper[j, k], keyProbas[j, k]!!),
                            SBOX_TABLES.relationDDT2,
                            LARGE_ALGO
                        ).post()
                        if (k in ATTACK_KEY_COLS) {
                            objectiveBounds += SBOX_TABLES.relationDDT2Bounds
                            keyBounds += SBOX_TABLES.relationDDT2Bounds
                        }
                        activeKeyTable[j, k] = BoomerangTable.DDT2
                    }

                    if (isDDT2Lower(ΔWKupper, freeWKupper, freeSWKupper, ΔWKlower, freeWKlower, freeSWKlower)) {
                        keyProbas[j, k] = intVar(SBOX_TABLES.relationDDT2Bounds)
                        model.table(
                            arrayOf(δWKlower[j, k], δSWKlower[j, k], keyProbas[j, k]!!),
                            SBOX_TABLES.relationDDT2,
                            LARGE_ALGO
                        ).post()
                        if (k in ATTACK_KEY_COLS) {
                            objectiveBounds += SBOX_TABLES.relationDDT2Bounds
                            keyBounds += SBOX_TABLES.relationDDT2Bounds
                        }
                        activeKeyTable[j, k] = BoomerangTable.DDT2
                    }

                    if (isBCT(ΔWKupper, freeWKupper, freeSWKupper, ΔWKlower, freeWKlower, freeSWKlower)) {
                        keyProbas[j, k] = intVar(SBOX_TABLES.relationBCTBounds)
                        model.table(
                            arrayOf(δWKupper[j, k], δSWKlower[j, k], keyProbas[j, k]!!),
                            SBOX_TABLES.relationBCT,
                            LARGE_ALGO
                        ).post()
                        if (k in ATTACK_KEY_COLS) {
                            objectiveBounds += SBOX_TABLES.relationBCTBounds
                            keyBounds += SBOX_TABLES.relationBCTBounds
                        }
                        activeKeyTable[j, k] = BoomerangTable.BCT
                    }

                    if (isUBCT(ΔWKupper, freeWKupper, freeSWKupper, ΔWKlower, freeWKlower, freeSWKlower)) {
                        keyProbas[j, k] = intVar(SBOX_TABLES.relationUBCTBounds)
                        model.table(
                            arrayOf(δWKupper[j, k], δSWKupper[j, k], δSWKlower[j, k], keyProbas[j, k]!!),
                            SBOX_TABLES.relationUBCT,
                            LARGE_ALGO
                        ).post()
                        if (k in ATTACK_KEY_COLS) {
                            objectiveBounds += SBOX_TABLES.relationUBCTBounds
                            keyBounds += SBOX_TABLES.relationUBCTBounds
                        }
                        activeKeyTable[j, k] = BoomerangTable.UBCT
                    }

                    if (isLBCT(ΔWKupper, freeWKupper, freeSWKupper, ΔWKlower, freeWKlower, freeSWKlower)) {
                        keyProbas[j, k] = intVar(SBOX_TABLES.relationLBCTBounds)
                        model.table(
                            arrayOf(δWKupper[j, k], δWKlower[j, k], δSWKlower[j, k], keyProbas[j, k]!!),
                            SBOX_TABLES.relationLBCT,
                            LARGE_ALGO
                        ).post()
                        if (k in ATTACK_KEY_COLS) {
                            objectiveBounds += SBOX_TABLES.relationLBCTBounds
                            keyBounds += SBOX_TABLES.relationLBCTBounds
                        }
                        activeKeyTable[j, k] = BoomerangTable.LBCT
                    }

                    if (isEBCT(ΔWKupper, freeWKupper, freeSWKupper, ΔWKlower, freeWKlower, freeSWKlower)) {
                        keyProbas[j, k] = intVar(SBOX_TABLES.relationEBCTBounds)
                        model.table(
                            arrayOf(
                                δWKupper[j, k],
                                δSWKupper[j, k],
                                δWKlower[j, k],
                                δSWKlower[j, k],
                                keyProbas[j, k]!!
                            ),
                            SBOX_TABLES.relationEBCT,
                            LARGE_ALGO
                        ).post()
                        if (k in ATTACK_KEY_COLS) {
                            objectiveBounds += SBOX_TABLES.relationEBCTBounds
                            keyBounds += SBOX_TABLES.relationEBCTBounds
                        }
                        activeKeyTable[j, k] = BoomerangTable.EBCT
                    }
                }
            }
        }


        for (ik in Nk until Nb * (Nr + 1)) {
            if (ik % Nk == 0) {
                for (j in 0..3) {
                    postXor(δWKupper[j, ik], δWKupper[j, ik - Nk], δSWKupper[(j + 1) % 4, ik - 1]!!)
                    postXor(δWKlower[j, ik], δWKlower[j, ik - Nk], δSWKlower[(j + 1) % 4, ik - 1]!!)
                }
            } else if (Nk > 6 && ik % Nk == 4) {
                for (j in 0..3) {
                    postXor(δWKupper[j, ik], δWKupper[j, ik - Nk], δSWKupper[j, ik - 1]!!)
                    postXor(δWKlower[j, ik], δWKlower[j, ik - Nk], δSWKlower[j, ik - 1]!!)
                }
            } else {
                for (j in 0..3) {
                    postXor(δWKupper[j, ik], δWKupper[j, ik - Nk], δWKupper[j, ik - 1])
                    postXor(δWKlower[j, ik], δWKlower[j, ik - Nk], δWKlower[j, ik - 1])
                }
            }
        }

        // ShiftRows
        δYupper = Tensor3(Nr, 4, Nb) { i, j, k -> δSXupper[i, j, (k + step1.config.shift[j]) % Nb] }
        δYlower = Tensor3(Nr, 4, Nb) { i, j, k -> δSXlower[i, j, (k + step1.config.shift[j]) % Nb] }

        // ARK
        δZupper = byteVars("δZ↑", step1.Z, TrailVar::upper)
        δZlower = byteVars("δZ↓", step1.Z, TrailVar::lower)

        // MixColumn
        for (i in 0 until Nr - 1) {
            mixColumn(δYupper[i], δZupper[i])
            mixColumn(δYlower[i], δZlower[i])
        }

        for (i in 0 until Nr - 1) {
            for (j in 0 until 4) {
                for (k in 0 until Nb) {
                    postXor(δXupper[i + 1, j, k], δZupper[i, j, k], δWKupper[j, (i + 1) * Nb + k])
                    postXor(δXlower[i + 1, j, k], δZlower[i, j, k], δWKlower[j, (i + 1) * Nb + k])
                }
            }
        }

        objective = intVar("step2obj", objectiveBounds)
        keyProba = intVar("keyProba", keyBounds)
        model.sum(keyProbas.deepFlatten().filterNotNull().toTypedArray(), "=", keyProba).post()
        twoT = intVar("2t", twoTBounds)

        rb = step1.rb
        rf = step1.rf
        val tb = rb / 8 * 7
        val tf = rf / 8 * 7

        val keyBits = step1.config.keySize.nbBits
        val blockBits = step1.config.textSize.nbBits

        val keyBitsMinusKeyProba = model.intVar("ks - keyProba",0, keyBits)
        model.arithm(keyProba, "+", keyBitsMinusKeyProba, "=", keyBits).post()

        val dataComplexity = model.intVar("DATA_COMPLEXITY", 0, min(keyBits, blockBits))
        model.min(dataComplexity, keyBitsMinusKeyProba, model.intVar(blockBits)).post()

        val twoTArray = probas[ATTACK_ROUNDS].deepFlatten().filterNotNull().toTypedArray()
        if (twoTArray.size != 0) {
            model.sum(twoTArray, "=", twoT).post()
//            model.arithm(twoT + (2 + blockBits), "<", 2 * dataComplexity).post()
/*
            if (step1.ATTACK_I) {
                model.arithm(twoT, "<", 2 * keyBitsMinusKeyProba - (blockBits + 2 + 2 * rb + 2 * rf)).post()
            } else if (step1.ATTACK_II) {
                model.arithm(twoT, "<", keyBitsMinusKeyProba - (1 + rf)).post()
                model.arithm(twoT, "<", keyBitsMinusKeyProba - (2 - tf)).post()
                model.arithm(twoT, "<", keyBitsMinusKeyProba + (blockBits - (2 * tf + 2 * rb))).post()
                model.arithm(twoT, "<", keyBitsMinusKeyProba + (blockBits - (1 + rb + tb + 2 * tf))).post()
                model.arithm(twoT, "<", keyBitsMinusKeyProba + (blockBits - (1 + rf + 2 * tb + tf))).post()
            } else if (step1.ATTACK_III) {
                model.arithm(twoT, "<", 2 * keyBitsMinusKeyProba - (2 * rb + 2 + blockBits)).post()
                model.arithm(twoT, "<", keyBitsMinusKeyProba - (rb - blockBits + 2 * rf)).post()
            }
*/
            val σ = 1
            val t = model.intVar("t",0, 1024)
            // 2t = ⌈ t/2 ⌉
            model.table(twoT, t, ceilingDiv(2, 1024)).post()
            val attack_I = model.intVar("ATTACK_I", 0, 1024)
            model.arithm(attack_I, "=", t, "+", rf + 2 + σ + blockBits / 2 + rb).post()

            val attack_II_0 = model.intVar("ATTACK_II_0", 0, 1024)
            model.arithm(attack_II_0, "=", twoT, "+", rf - 1 + 2 * σ).post()

            val attack_II_1 = model.intVar("ATTACK_II_1", 0, 1024)
            model.arithm(attack_II_1, "=", twoT, "+", tf + 2 * σ).post()

            val attack_II_2 = model.intVar("ATTACK_II_2", 0, 1024)
            model.arithm(attack_II_2, "=", twoT, "+", 2 * tf + 2 * rb - blockBits - 2 + 2 * σ).post()

            val attack_II_3 = model.intVar("ATTACK_II_3", 0, 1024)
            model.arithm(attack_II_3, "=", twoT, "+", rb + tb + 2 * tf - blockBits - 1 + 2 * σ).post()

            val attack_II_4 = model.intVar("ATTACK_II_4", 0, 1024)
            model.arithm(attack_II_4, "=", twoT, "+", rf + 2 * tb + tf - blockBits - 1 + 2 * σ).post()

            val attack_II_5 = model.intVar("ATTACK_II_5", 0, 1024)
            model.arithm(attack_II_5, "=", t, "+", σ + blockBits / 2).post()

            val attack_II = model.intVar("ATTACK_II", 0, 1024)
            model.max(attack_II, arrayOf(attack_II_0, attack_II_1, attack_II_2, attack_II_3, attack_II_4, attack_II_5))
                .post()

            val attack_III_0 = model.intVar("ATTACK_III", 0, 1024)
            model.arithm(attack_III_0, "=", t, "+", σ + blockBits / 2 + rb).post()

            val attack_III_1 = model.intVar("ATTACK_III_0", 0, 1024)
            model.arithm(attack_III_1, "=", twoT, "+", rb - blockBits + 2 * rf).post()

            val attack_III = model.intVar("ATTACK_III_0", 0, 1024)
            model.max(attack_III, arrayOf(attack_III_0, attack_III_1)).post()

            timeComplexity = model.intVar("TimeComplexity",0, 1024)
            model.min(timeComplexity, arrayOf(attack_I, attack_II, attack_III)).post()
        } else {
            timeComplexity = model.intVar(0, 0)
        }
        model.setObjective(Model.MINIMIZE, timeComplexity)

        val middleVars = mutableListOf<IntVar>()
        val ddtVars = mutableListOf<IntVar>()
        val probaVars = mutableListOf<IntVar>()

        for (i in 0 until Nr) {
            for (j in 0..3) {
                for (k in 0 until Nb) {
                    if (δXupper[i, j, k] != FREE && δSXlower[i, j, k] != FREE) {
                        val p = probas[i, j, k]
                        if (p != null) {
                            middleVars += δXupper[i, j, k]
                            middleVars += δSXlower[i, j, k]
                            if (δSXupper[i, j, k] != FREE) {
                                middleVars += δSXupper[i, j, k]
                            }
                            if (δXlower[i, j, k] != FREE) {
                                middleVars += δXlower[i, j, k]
                            }

                            middleVars += p
                            if (i in ATTACK_ROUNDS) {
                                probaVars += p
                            }
                        }

                    } else {
                        val p = probas[i, j, k]
                        if (p != null) {
                            if (δXupper[i, j, k] != FREE)
                                ddtVars.add(δXupper[i, j, k]);
                            if (δSXupper[i, j, k] != FREE)
                                ddtVars.add(δSXupper[i, j, k]);
                            if (δXlower[i, j, k] != FREE)
                                ddtVars.add(δXlower[i, j, k]);
                            if (δSXlower[i, j, k] != FREE)
                                ddtVars.add(δSXlower[i, j, k]);

                            ddtVars += p
                            if (i in ATTACK_ROUNDS) {
                                probaVars += p
                            }
                        }
                    }

                    val ik = i * Nb + k
                    if (step1.config.isSbColumn(ik)) {
                        if (δWKupper[j, ik] != FREE && δWKlower[j, ik] != FREE) {
                            val kp = keyProbas[j, ik]
                            if (kp != null) {
                                middleVars += δWKupper[j, ik]
                                middleVars += δWKlower[j, ik]
                                if (δSWKupper[j, ik] != FREE) {
                                    middleVars += δSWKupper[j, ik]!!
                                }
                                if (δSWKlower[j, ik] != FREE) {
                                    middleVars += δSWKlower[j, ik]!!
                                }

                                middleVars += kp
                                if (ik in ATTACK_KEY_COLS) {
                                    probaVars += kp
                                }
                            }
                        } else {
                            val kp = keyProbas[j, ik]
                            if (kp != null) {
                                if (δWKupper[j, ik] != FREE)
                                    ddtVars.add(δWKupper[j, ik]);
                                if (δSWKupper[j, ik] != FREE)
                                    ddtVars.add(δSWKupper[j, ik]!!);
                                if (δWKlower[j, ik] != FREE)
                                    ddtVars.add(δWKlower[j, ik]);
                                if (δSWKlower[j, ik] != FREE)
                                    ddtVars.add(δSWKlower[j, ik]!!);

                                middleVars += kp
                                if (ik in ATTACK_KEY_COLS) {
                                    probaVars += kp
                                }
                            }
                        }
                    }
                }
            }
        }

        check(FREE.propagators.isEmpty() || FREE.propagators.all { it == null })
        check(ZERO.propagators.isEmpty() || ZERO.propagators.all { it == null })

        middleVars.removeAll { it == ZERO || it == ZERO }
        ddtVars.removeAll { it == ZERO || it == FREE }

        logger.log("Tables distribution: %s",
            (activeTable.deepFlatten() + activeKeyTable.deepFlatten()).groupBy { it }
                .map { (k, v) -> k to v.size }
        )

        model.sum(probaVars.toTypedArray(), "=", objective).post()
        for (chunk in probaVars.toList().chunks(10)) {
            subsets(chunk) { subset ->
                if (subset.size >= 2) {
                    model.sum(subset.toTypedArray(), "<=", objective).post()
                }
            }
        }

        val solver = model.solver

        var decisionVars = middleVars + ddtVars
        if (decisionVars.isEmpty()) {
            decisionVars = model.retrieveIntVars(true).toList()
        }
        logger.debug("Search: %s", decisionVars.map(Variable::getName))

        DefaultSearchConfigurer.configureSearch(solver, decisionVars.toTypedArray(), 5, true, true);
    }

    private fun intVar(bounds: Bounds) = intVar(bounds.min, bounds.max)
    private fun intVar(name: String, bounds: Bounds) = intVar(name, bounds.min, bounds.max)

    private fun byteVars(
        name: String,
        abstractState: Tensor3<BoomerangSbVar>,
        trail: (TrailVar) -> Int,
        freeSelector: (BoomerangSbVar) -> TrailVar
    ) =
        Tensor3(abstractState.dim1, abstractState.dim2, abstractState.dim3) { i, j, k ->
            if (trail(freeSelector(abstractState[i, j, k])) == 0) {
                if (trail(abstractState[i, j, k].Δ) == 1) {
                    intVar("$name[$i, $j, $k]", 1, 255)
                } else {
                    ZERO
                }
            } else {
                FREE
            }
        }

    private fun byteVars(name: String, state: Tensor3<BoomerangLinVar>, trail: (TrailVar) -> Int) =
        Tensor3(state.dim1, state.dim2, state.dim3) { i, j, k ->
            if (trail(state[i, j, k].free) == 0) {
                if (trail(state[i, j, k].Δ) == 1) {
                    intVar("$name[$i, $j]", 1, 255)
                } else {
                    ZERO
                }
            } else {
                FREE
            }
        }

    private fun byteVars(name: String, state: Matrix<BoomerangOptionalSbVar>, trail: (TrailVar) -> Int) =
        Matrix(state.dim1, state.dim2) { i, j ->
            if (trail(state[i, j].free) == 0) {
                if (trail(state[i, j].Δ) == 1) {
                    intVar("$name[$i, $j]", 1, 255)
                } else {
                    ZERO
                }
            } else {
                FREE
            }
        }

    private fun byteSBVars(name: String, state: Matrix<BoomerangOptionalSbVar>, trail: (TrailVar) -> Int) =
        Matrix(state.dim1, state.dim2) { i, j ->
            if (step1.config.isSbColumn(j)) {
                if (trail(state[i, j].freeS!!) == 0) {
                    if (trail(state[i, j].Δ) == 1) {
                        intVar("$name[$i, $j]", 1, 255)
                    } else {
                        ZERO
                    }
                } else {
                    FREE
                }

            } else null
        }

    private fun mixColumn(δY: Matrix<IntVar>, δZ: Matrix<IntVar>) {
        val δY2 = Matrix(4, Nb) { j, k -> galoisFieldMul(δY[j, k], 2) }
        val δY3 = Matrix(4, Nb) { j, k -> galoisFieldMul(δY[j, k], 3) }

        for (k in 0 until Nb) {
            postXor(δZ[0, k], xor(δY2[0, k], δY3[1, k]), xor(δY[2, k], δY[3, k]))
            postXor(δZ[1, k], xor(δY[0, k], δY2[1, k]), xor(δY3[2, k], δY[3, k]))
            postXor(δZ[2, k], xor(δY[0, k], δY[1, k]), xor(δY2[2, k], δY3[3, k]))
            postXor(δZ[3, k], xor(δY3[0, k], δY[1, k]), xor(δY[2, k], δY2[3, k]))
        }

        val δZ09 = Matrix(4, Nb) { j, k -> galoisFieldMul(δZ[j, k], 9) }
        val δZ11 = Matrix(4, Nb) { j, k -> galoisFieldMul(δZ[j, k], 11) }
        val δZ13 = Matrix(4, Nb) { j, k -> galoisFieldMul(δZ[j, k], 13) }
        val δZ14 = Matrix(4, Nb) { j, k -> galoisFieldMul(δZ[j, k], 14) }

        for (k in 0 until Nb) {
            postXor(δY[0, k], xor(δZ14[0, k], δZ11[1, k]), xor(δZ13[2, k], δZ09[3, k]))
            postXor(δY[1, k], xor(δZ09[0, k], δZ14[1, k]), xor(δZ11[2, k], δZ13[3, k]))
            postXor(δY[2, k], xor(δZ13[0, k], δZ09[1, k]), xor(δZ14[2, k], δZ11[3, k]))
            postXor(δY[3, k], xor(δZ11[0, k], δZ13[1, k]), xor(δZ09[2, k], δZ14[3, k]))
        }
    }

    private fun galoisFieldMul(v: IntVar, k: Int): IntVar {
        if (v == FREE) {
            return FREE
        }
        if (v == ZERO) {
            return ZERO
        }

        val tuples = when (k) {
            2 -> mul2
            3 -> mul3
            9 -> mul9
            11 -> mul11
            13 -> mul13
            14 -> mul14
            else -> panic("Invalid coefficient $k in MC ")
        }

        val res = intVar("$k${v.name}", 0, 255)
        table(v, res, tuples, BIN_ALGO).post()
        return res
    }

    private fun postXor(a: IntVar, b: IntVar, c: IntVar) {
        if (a == FREE || b == FREE || c == FREE) {
            return
        }
        if (a == ZERO && b == ZERO && c == ZERO) {
            return
        }
        if (a == ZERO && b == ZERO) {
            arithm(c, "=", 0).post(); return
        }
        if (b == ZERO && c == ZERO) {
            arithm(a, "=", 0).post(); return
        }
        if (c == ZERO && a == ZERO) {
            arithm(b, "=", 0).post(); return
        }
        if (a == ZERO) {
            arithm(b, "=", c).post(); return
        }
        if (b == ZERO) {
            arithm(a, "=", c).post(); return
        }
        if (c == ZERO) {
            arithm(a, "=", b).post(); return
        }
        table(arrayOf(a, b, c), XOR_TUPLES, "FC").post()
    }

    private fun Tensor3<IntVar>.values(): IntTensor3 = IntTensor3(dim1, dim2, dim3) { i, j, k -> this[i, j, k].value }
    private fun Tensor3<IntVar?>.values(): Tensor3<Int?> = Tensor3(dim1, dim2, dim3) { i, j, k -> this[i, j, k]?.value }

    private fun Matrix<IntVar>.values(): IntMatrix = IntMatrix(dim1, dim2) { i, j -> this[i, j].value }
    private fun Matrix<IntVar?>.values(): Matrix<Int?> = Matrix(dim1, dim2) { i, j -> this[i, j]?.value }

    private fun xor(lhs: IntVar, rhs: IntVar): IntVar {
        if (lhs == FREE || rhs == FREE) {
            return FREE
        }
        if (lhs == ZERO && rhs == ZERO) {
            return ZERO
        }
        if (lhs == ZERO) return rhs
        if (rhs == ZERO) return lhs

        val tmp = intVar("${lhs.name} ^ ${rhs.name}", 0, 255)
        table(arrayOf(lhs, rhs, tmp), XOR_TUPLES, "FC").post()
        return tmp
    }

    operator fun IntVar.minus(other: IntVar): IntVar {
        val res = model.intVar(lb - other.ub, ub - other.lb)
        model.arithm(res, "=", this, "-", other).post();
        return res
    }

    operator fun Int.times(v: IntVar) = v.times(this)
    operator fun IntVar.times(k: Int): IntVar {
        val key = "$k x ${this.name}"
        if (MEM[key] == null) {
            val newVar = if (k >= 0) model.intVar(lb * k, ub * k) else model.intVar(ub * k, lb * k)
            MEM[key] = newVar
            // this * k = newVar <=> k = newVar / this
            model.arithm(newVar, "/", this, "=", k).post()
        }

        return MEM[key]!!
    }

    operator fun Int.plus(v: IntVar) = v.plus(this)
    operator fun IntVar.plus(k: Int): IntVar {
        val key = "$k + ${this.name}"
        if (MEM[key] == null) {
            val newVar = model.intVar(lb + k, ub + k)
            MEM[key] = newVar
            // newVar = this + k <=> k = newVar - this
            model.arithm(newVar, "-", this, "=", k).post()
        }

        return MEM[key]!!
    }

    operator fun Int.minus(v: IntVar) = (v * -1).plus(this)
    operator fun IntVar.minus(k: Int): IntVar {
        val key = "$k - ${this.name}"
        if (MEM[key] == null) {
            val newVar = model.intVar(lb - k, ub - k)
            MEM[key] = newVar
            // newVar = this - k <=> k = newVar - this
            model.arithm(this, "-", newVar, "=", k).post()
        }

        return MEM[key]!!
    }


    override fun into(): OptimizeRkStep2Solution = OptimizeRkStep2Solution(
        step1.config, objective.value,
        δXupper.values(), δSXupper.values(), δYupper.values(), δZupper.values(), δWKupper.values(), δSWKupper.values(),
        δXlower.values(), δSXlower.values(), δYlower.values(), δZlower.values(), δWKlower.values(), δSWKlower.values(),
        probas.values(), activeTable, keyProbas.values(), activeKeyTable
    )


    private fun ceilingDiv(k: Int, max: Int): Tuples {
        val result = Tuples(true)
        for (i in 0..max) {
            result.add(i, i / k + if (i % k != 0) 1 else 0)
        }
        return result
    }

}