package com.github.rloic.phd.boomerang.tmp

import com.github.rloic.phd.core.mzn.FznModel
import com.github.rloic.phd.infra.MiniZincBinary
import com.github.rloic.phd.infra.PicatBinary
import java.io.File

val MZN_VERSION = "2.5.5"
val PICAT_VERSION = "3.2"

fun main() {
    val minizinc = MiniZincBinary("/home/rloic/.local/libraries/MiniZinc/$MZN_VERSION/bin/minizinc")
    val picat = PicatBinary(
        "/home/rloic/.local/libraries/Picat/$PICAT_VERSION/picat",
        "/home/rloic/.local/libraries/Picat/$PICAT_VERSION/lib/fzn_picat_sat.pi",
        minizinc
    )



    println(picat.buildCommand(FznModel(File("/home/rloic/Travail/Code/IdeaProjects/step2marine/mzn_models/tmp/step1-opt-1-10-0.fzn"))).joinToString(" "))
}