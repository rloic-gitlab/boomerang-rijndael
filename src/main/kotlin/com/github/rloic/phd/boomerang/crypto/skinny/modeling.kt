package comb.github.rloic.boomerang.sat.skinny

import com.github.rloic.phd.boomerang.crypto.skinny.Mod

fun lanes(R: Int, trail: String, mod: Mod) = buildString {
    if (mod < Mod.TK1) throw IllegalArgumentException("Cannot create lanes for SK or TK1.")

    val invPermutationTKS = intArrayOf(2, 0, 4, 7, 6, 3, 5, 1)
    val evenLaneSize = R / 2
    val oddLaneSize = (R - 1) / 2
    var currentPos: Int
    var currentLane: String
    for (i in 0 until 8) {
        currentPos = i
        currentLane = "lanes%s[%d]".format(trail, i * 2)
        val evenLane = mutableListOf<String>()
        for (round in 0 until R - 1 step 2) {
            val evenLaneVar = "DTK%s[%d, %d, %d]".format(trail, round, currentPos / 4, currentPos % 4)
            evenLane += evenLaneVar
            appendLine("constraint $evenLaneVar -> $currentLane;")
            currentPos = invPermutationTKS[currentPos]
        }
        appendLine("constraint ${evenLane.joinToString(" + ")} >= ${evenLaneSize - (mod.ordinal - 1)} * $currentLane;")

        currentPos = i
        currentLane = "lanes%s[%d]".format(trail, i * 2 + 1)
        val oddLane = mutableListOf<String>()
        for (round in 1 until R - 1 step 2) {
            val oddLaneVar = "DTK%s[%d, %d, %d]".format(trail, round, currentPos / 4, currentPos % 4)
            oddLane += oddLaneVar
            appendLine("constraint $oddLaneVar -> $currentLane;")
            currentPos = invPermutationTKS[currentPos]
        }
        appendLine("constraint ${oddLane.joinToString(" + ")} >= ${oddLaneSize - (mod.ordinal - 1)} * $currentLane;")
    }
}