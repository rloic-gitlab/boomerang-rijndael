package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.parsers

import com.github.rloic.phd.core.cryptography.ciphers.rijndael.RkRijndael
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.OptimizeRijndaelBoomerangRkStep1Solution
import com.github.rloic.phd.core.mzn.MznSolution
import com.github.rloic.phd.core.mzn.MznSolutionParser

@Suppress("NonAsciiCharacters")
class OptimizeRkStep1SolutionParser(val config: RkRijndael) : MznSolutionParser<OptimizeRijndaelBoomerangRkStep1Solution> {
    private val delegate = EnumerateRkStep1SolutionParser(config)
    override fun parse(serialized: MznSolution): OptimizeRijndaelBoomerangRkStep1Solution {
        val elements = serialized.content.split('\n')
        val solution = delegate.parse(serialized)
        val objStep1 = elements.getIntValueOr("objective", solution.twoT + solution.keyProba)
        return OptimizeRijndaelBoomerangRkStep1Solution(solution.config, objStep1, solution.X, solution.Y, solution.Z, solution.WK, solution.ATTACK_I, solution.ATTACK_II, solution.ATTACK_III, solution.keyProba, solution.twoT, solution.rb, solution.rf, solution.timeComplexity)
    }
}