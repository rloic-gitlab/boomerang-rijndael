package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.singlekey.step1

import com.github.rloic.phd.boomerang.crypto.rijndael.Mod
import com.github.rloic.phd.boomerang.crypto.rijndael.RijndaelModels
import com.github.rloic.phd.boomerang.crypto.rijndael.Tmp
import com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.parsers.EnumerateSkStep1SolutionParser
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.SkRijndael
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.presenters.SkStep1SolutionLatexPresenter
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.EnumerateRijndaelBoomerangSkStep1Solution
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.differential.solutions.EnumerateSkStep1Solution
import com.github.rloic.phd.core.error.panic
import com.github.rloic.phd.core.files.latex.Latex
import com.github.rloic.phd.core.mzn.*
import com.github.rloic.phd.core.utils.expectArgument
import com.github.rloic.phd.core.utils.parseArgs
import com.github.rloic.phd.infra.MiniZincBinary
import com.github.rloic.phd.infra.PicatBinary
import javaextensions.io.div
import javaextensions.io.mkdirs
import javaextensions.util.map

fun main(_args_: Array<String>) {
    val args = parseArgs(_args_)

    val minizinc = MiniZincBinary.from(args)
    val picat = PicatBinary.from(args, minizinc)
    val configuration = SkRijndael.from(args)
    val objStep1 = args.expectArgument("ObjStep1").toInt()

    var nbSolutions = 0
    for (solution in enumerateSk(picat, configuration, objStep1)) {
        Latex.preview(::SkStep1SolutionLatexPresenter, solution)
        nbSolutions += 1
    }
    println(nbSolutions)
}

fun enumerateSk(solver: MznSolver, config: SkRijndael, objStep1: Int): Iterator<EnumerateRijndaelBoomerangSkStep1Solution> {
    val tmpFolder = mkdirs(Tmp / Mod.SingleKey / config.textSize.nbBits) ?: panic()

    val mznModel = MznModelBuilder.PartialAssignment(
        listOf(
            PartialMznModel(RijndaelModels / "Main.mzn"),
            PartialMznModel(RijndaelModels / "${Mod.SingleKey}.mzn"),
        ),
        MznSearchConfiguration.from(PartialMznModel(RijndaelModels / "SKStep1DecisionVars.mzn"),
        SearchStrategy.Smallest, ValueSelector.InDomainMin),
        PartialMznModel(RijndaelModels / "ForbidSKStep1Solution.mzn"),
        RijndaelSkPartialAssignment(config)::extractSolution
    ).build(tmpFolder / "step1_enumerate_${config.Nr}.mzn")

    val data = mapOf(
        "Nr" to config.Nr,
        "BLOCK_BITS" to config.textSize.nbBits,
        "objective" to objStep1
    )

    return solver.enumerate(mznModel, data).map(EnumerateSkStep1SolutionParser(config)::parse)
}