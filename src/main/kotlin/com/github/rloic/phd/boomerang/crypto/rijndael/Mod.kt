package com.github.rloic.phd.boomerang.crypto.rijndael

enum class Mod { SingleKey, RelatedKey }