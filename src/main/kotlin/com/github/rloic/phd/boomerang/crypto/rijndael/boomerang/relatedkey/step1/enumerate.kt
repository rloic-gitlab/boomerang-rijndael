package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step1

import com.github.rloic.phd.boomerang.crypto.rijndael.Mod
import com.github.rloic.phd.boomerang.crypto.rijndael.RijndaelModels
import com.github.rloic.phd.boomerang.crypto.rijndael.Tmp
import com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.parsers.EnumerateRkStep1SolutionParser
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.RkRijndael
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.presenters.RkStep1SolutionLatexPresenter
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.EnumerateRijndaelBoomerangRkStep1Solution
import com.github.rloic.phd.core.error.panic
import com.github.rloic.phd.core.files.latex.Latex
import com.github.rloic.phd.core.mzn.*
import com.github.rloic.phd.core.utils.expectArgument
import com.github.rloic.phd.core.utils.parseArgs
import com.github.rloic.phd.infra.MiniZincBinary
import com.github.rloic.phd.infra.PicatBinary
import comb.github.rloic.boomerang.sat.rijndael.utils.KeySchedule
import javaextensions.io.div
import javaextensions.io.mkdirs
import javaextensions.util.map

fun main(_args_: Array<String>) {
    val args = parseArgs(_args_)

    val minizinc = MiniZincBinary.from(args)
    val picat = PicatBinary.from(args, minizinc)
    val configuration = RkRijndael.from(args)
    val objStep1 = args.expectArgument("ObjStep1").toInt()


    var nbSolutions = 0
    for (solution in enumerateRk(picat, configuration, objStep1)) {
        Latex.preview(::RkStep1SolutionLatexPresenter, solution)
        nbSolutions += 1
        println(nbSolutions)
    }
    println(nbSolutions)
}

fun enumerateRk(solver: MznSolver, config: RkRijndael, objStep1: Int): Iterator<EnumerateRijndaelBoomerangRkStep1Solution> {
    val tmpFolder = mkdirs(Tmp / Mod.RelatedKey / config.textSize.nbBits / config.keySize.nbBits) ?: panic()

    val searchConfiguration = RawMznSearchConfiguration("""
       solve :: int_search(
          %s
          , dom_w_deg
          , indomain_min
          , complete
       ) minimize number_of_active_sboxes; 
    """.format((RijndaelModels / "RKStep1DecisionVars.mzn").readText()))

    val mznModel = MznModelBuilder.PartialAssignment(
        listOf(
            PartialMznModel(RijndaelModels / "Main.mzn"),
            PartialMznModel(RijndaelModels / "${Mod.RelatedKey}.mzn")
        ),
        searchConfiguration,
        PartialMznModel(RijndaelModels / "ForbidRKStep1Solution.mzn"),
        RijndaelRkPartialAssignment(config)::extractSolution
    ).build(tmpFolder / "step1_enumerate_${config.Nr}.mzn")

    KeySchedule.generate(config, "upper", mznModel)
    KeySchedule.generate(config, "lower", mznModel)

    val data = mapOf(
        "Nr" to config.Nr,
        "BLOCK_BITS" to config.textSize.nbBits,
        "KEY_BITS" to config.keySize.nbBits,
        "time_complexity" to objStep1
    )

    return solver.enumerate(mznModel, data)
        .map(EnumerateRkStep1SolutionParser(config)::parse)
}
