package com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step2

import com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step1.enumerateRk
import com.github.rloic.phd.boomerang.crypto.rijndael.boomerang.relatedkey.step1.optimizeRk
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.RkRijndael
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.presenters.RkStep1SolutionLatexPresenter
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.presenters.RkStep2SolutionLatexPresenter
import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.solutions.OptimizeRkStep2Solution
import com.github.rloic.phd.core.files.latex.Latex
import com.github.rloic.phd.core.utils.Logger
import com.github.rloic.phd.core.utils.elapsed
import com.github.rloic.phd.core.utils.logger
import com.github.rloic.phd.core.utils.parseArgs
import com.github.rloic.phd.infra.MiniZincBinary
import com.github.rloic.phd.infra.PicatBinary
import java.io.File
import java.time.Duration
import java.time.Instant
import kotlin.math.absoluteValue

fun main(_args_: Array<String>) {
    val args = parseArgs(_args_)
    val minizinc = MiniZincBinary.from(args)
    val picat = PicatBinary.from(args, minizinc)
    val step1Config = RkRijndael.from(args)

    logger = Logger.from(args)
    logger.addTerminal()

    var step1OptTime = Duration.ZERO
    var step1EnumTime = Duration.ZERO
    var step2OptTime = Duration.ZERO

    var objStep1Opt = args["ObjStep1"]?.toInt()
    if (objStep1Opt == null) {
        logger.info("ObjStep1 is not given. Computing ObjStep1Opt...")
        val startStep1Opt = Instant.now()

        val step1Sol = optimizeRk(picat, RkRijndael.from(args))!!
        Latex.preview(::RkStep1SolutionLatexPresenter, step1Sol)
        objStep1Opt = step1Sol.timeComplexity
        logger.log("Time to compute Step1Opt = %.2fs", startStep1Opt.elapsed().toMillis() / 1000.0)
        step1OptTime += startStep1Opt.elapsed()
    } else {
        logger.trace("ObjStep1 = ${objStep1Opt.absoluteValue}")
        logger.info("Skipping Step1Opt")
    }

    var upperBound: Int? = args["UpperBound"]?.toInt()
    logger.trace("User defined UpperBound = $upperBound")
    fun upperBound() = upperBound ?: step1Config.keySize.nbBits
    logger.trace("Updated UpperBound = 2^{${upperBound()}}")

    var lowerBound = objStep1Opt
    logger.trace("Lower bound = 2^{$lowerBound}")
    val limit = args["Limit"]?.toInt()
    logger.trace("Limit = $limit")

    val step2Output = args["Step2OutFile"]
    logger.trace("Step2OutFile = $step2Output")
    var iterator = enumerateRk(picat, step1Config, lowerBound)

    var counter = 0
    while (lowerBound < upperBound()) {
        logger.log("$step1Config UpperBound: 2^{${upperBound()}} LowerBound: 2^{${lowerBound}}")
        val step1Start = Instant.now()
        if ((limit == null || counter < limit) && iterator.hasNext()) {
            logger.log("Time to solve Step1 = %.2fs", step1Start.elapsed().toMillis() / 1000.0)
            logger.log("New Step1 solution has been found")
            step1EnumTime += step1Start.elapsed()
            counter += 1
            val step1Sol = iterator.next()

            val step2Sol = RijndaelRKStep2(step1Sol, upperBound())

            val step2Solver = step2Sol.model.solver
            val step2Start = Instant.now()
            // step2Solver.plugMonitor(ParetoOptimizer(false, arrayOf(step2Sol.keyProba, step2Sol.objective)))
            while (step2Solver.solve()) {
                counter = 0
                upperBound = step2Sol.objective.value

                /* step2Sol.model.or(
                    step2Sol.model.arithm(step2Sol.keyProba, "<", step2Sol.keyProba.value),
                    step2Sol.model.and(
                        step2Sol.model.arithm(step2Sol.keyProba, "=", step2Sol.keyProba.value),
                        step2Sol.model.arithm(step2Sol.twoT, "<", step2Sol.twoT.value)
                    )
                ).post() */

                logger.log("Time Complexity: 2^{-%d}", step2Sol.timeComplexity.value)
                logger.log("TextProba: 2^{-%d}", step2Sol.twoT.value)
                logger.log("KeyProba: 2^{-%d}", step2Sol.keyProba.value)
                logger.log("rb: %d", step2Sol.rb)
                logger.log("rf: %d", step2Sol.rf)
                logger.log("---")

                if (step2Output != null) {
                    val step2Solution = step2Sol.into()
                    val outFile = File(
                        step2Output
                            .replace("{Nr}", step1Config.Nr.toString())
                            .replace("{TextSize}", step1Config.textSize.nbBits.toString())
                            .replace("{KeySize}", step1Config.keySize.nbBits.toString())
                            .replace("{ObjStep2}", step2Solution.objStep2.toString())
                            .replace("{TextProba}", step2Sol.twoT.value.toString())
                            .replace("{KeyProba}", step2Sol.keyProba.value.toString())
                    )
                    outFile.parentFile?.mkdirs()
                    val solutionWriter =
                        RkStep2SolutionLatexPresenter<OptimizeRkStep2Solution>(outFile.bufferedWriter())
                    /* TextPresenter<OptimizeRkStep2Solution>("/home/rloic/Personnel/Code/CLion/compute-proba/solution/${outFile.nameWithoutExtension}").present(
                        step2Solution
                    ) */
                    solutionWriter.present(step2Solution)
                    solutionWriter.close()
                }
                step2Solver.printShortStatistics()
                logger.info("New best probability: 2^{$upperBound}")
            }
            step2Solver.printShortStatistics()
            logger.log("Time to solve Step2 = %.2fs", step2Start.elapsed().toMillis() / 1000.0)
            step2OptTime += step2Start.elapsed()
        } else {
            if (counter == limit) {
                logger.info("Iteration limit (Step1) for LowerBound = $lowerBound has been reached")
            } else {
                logger.log("Time to solve Step1 = %.2fs", step1Start.elapsed().toMillis() / 1000.0)
                logger.info("No more Step1 solution for LowerBound = $lowerBound")
            }
            step1EnumTime += step1Start.elapsed()
            lowerBound += 1
            logger.trace("LowerBound = $lowerBound")
            counter = 0
            iterator = enumerateRk(picat, step1Config, lowerBound)
        }
    }
    logger.log("End research")

    logger.log("\n\n")

    logger.log("$step1Config UpperBound: 2^{${upperBound()}} LowerBound: 2^{${lowerBound}}")
    logger.log("Step1OptTime, Step1EnumTime, Step2OptTime, ObjStep2")

    val p = if (upperBound != null) {
        "2^{$lowerBound}"
    } else {
        "NA"
    }
    logger.log(
        "%.2f,%.2f,%.2f,%s".format(
            step1OptTime.toMillis() / 1000.0,
            step1EnumTime.toMillis() / 1000.0,
            step2OptTime.toMillis() / 1000.0,
            p
        )
    )
}