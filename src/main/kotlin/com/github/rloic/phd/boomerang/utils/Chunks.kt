package com.github.rloic.phd.boomerang.utils


class Chunks<T>(
    private val v: List<T>,
    private val chunkSize: Int
): Iterator<List<T>> {

    init {
        if (chunkSize < 1) throw Exception("Invalid chunk size")
    }

    var index = 0

    override fun hasNext() = index < v.size

    override fun next(): List<T> {
        val size = kotlin.math.min(v.size - index, chunkSize)
        val res = List<T>(size) { v[index + it] }
        index += chunkSize
        return res
    }
}

fun <T> List<T>.chunks(n: Int) = Chunks(this, n)