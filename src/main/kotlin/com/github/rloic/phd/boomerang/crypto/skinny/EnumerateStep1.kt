package comb.github.rloic.boomerang.sat.skinny

/*
fun main(args: Array<String>) {
    val arguments = parseArgs(args)

    val Nr = arguments.expectArgument("Nr").toInt()
    if (Nr < 5) throw IllegalArgumentException("Nr must be greater or equals than 5.")

    val objective = arguments.expectArgument("ObjStep1").toInt()

    val mod = Mod.valueOf(arguments.expectArgument("Mod"))

    (TMP / mod).mkdirs()
    val mainModel = CURRENT_FOLDER / "main.mzn"
    val tmpModel = MznModel(mainModel.copyTo(TMP / mod / "step1_enumerate_$Nr.mzn", overwrite = true))
    val linearPart = CURRENT_FOLDER / "linear_$mod.mzn"
    tmpModel.append(linearPart)
    if (mod == TK2 || mod == TK3) {
        tmpModel.appendText(lanes(Nr, "upper", mod))
        tmpModel.appendText(lanes(Nr, "lower", mod))
    }

    tmpModel.appendText("\nconstraint (objective = $objective);")

    val env = Env()
    var picatModel = env.mzn2fzn(tmpModel, mapOf("R" to Nr))

    val parser = parse(Nr, mod)
    val solutions = env.orTools(picatModel, parser)

    var nbSols = 0
    val allSolutions = solutions.toMutableList()
    while (solutions.isNotEmpty() && nbSols < 20) {

        for (solution in solutions) {
            tmpModel.forbid(solution, nbSols++)
            println(solution.objective)
            println(nbSols)
        }
        solutions.clear()

        picatModel = env.mzn2fzn(tmpModel, mapOf("R" to Nr))
        solutions += env.orTools(picatModel, parser)
        allSolutions += solutions
    }

    val step2 = getStep2Solutions(allSolutions, 8, 0, SK)
    println()
}

fun MznModel.forbid(solution: Step1Solution, i: Int) {

    val AND = "/\\"

    appendLine("array[ROUNDS, ROWS, COLS] of var 0..1: DXupper_$i = array3d(ROUNDS, ROWS, COLS, " + flatten(flatten(solution.DXupper)).contentToString() + ");")
    appendLine("array[ROUNDS, ROWS, COLS] of var 0..1: freeXupper_$i = array3d(ROUNDS, ROWS, COLS, " + flatten(flatten(solution.freeXupper)).contentToString() + ");")
    appendLine("array[ROUNDS, ROWS, COLS] of var 0..1: freeSBupper_$i = array3d(ROUNDS, ROWS, COLS, " + flatten(flatten(solution.freeSBupper)).contentToString() + ");")
    appendLine("array[ROUNDS, ROWS, COLS] of var 0..1: DXlower_$i = array3d(ROUNDS, ROWS, COLS, " + flatten(flatten(solution.DXlower)).contentToString() + ");")
    appendLine("array[ROUNDS, ROWS, COLS] of var 0..1: freeXlower_$i = array3d(ROUNDS, ROWS, COLS, " + flatten(flatten(solution.freeXlower)).contentToString() + ");")
    appendLine("array[ROUNDS, ROWS, COLS] of var 0..1: freeSBlower_$i = array3d(ROUNDS, ROWS, COLS, " + flatten(flatten(solution.freeSBlower)).contentToString() + ");")
    appendLine("array[ROUNDS, ROWS, COLS] of var 0..1: isTable_$i = array3d(ROUNDS, ROWS, COLS, " + flatten(flatten(solution.isTable)).contentToString() + ");")
    appendLine("array[ROUNDS, ROWS, COLS] of var 0..1: isDDT2_$i = array3d(ROUNDS, ROWS, COLS, " + flatten(flatten(solution.isDDT2)).contentToString() + ");")
    appendLine("constraint not (")
    appendLine("  ( forall (i in ROUNDS, j in ROWS, k in COLS) (DXupper[i, j, k] != DXupper_$i[i, j, k]) ) $AND")
    appendLine("  ( forall (i in ROUNDS, j in ROWS, k in COLS) (freeXupper[i, j, k] != freeXupper_$i[i, j, k]) ) $AND")
    appendLine("  ( forall (i in ROUNDS, j in ROWS, k in COLS) (freeSBupper[i, j, k] != freeSBupper_$i[i, j, k]) ) $AND")
    appendLine("  ( forall (i in ROUNDS, j in ROWS, k in COLS) (DXlower[i, j, k] != DXlower_$i[i, j, k]) ) $AND")
    appendLine("  ( forall (i in ROUNDS, j in ROWS, k in COLS) (freeXlower[i, j, k] != freeXlower_$i[i, j, k]) ) $AND")
    appendLine("  ( forall (i in ROUNDS, j in ROWS, k in COLS) (freeSBlower[i, j, k] != freeSBlower_$i[i, j, k]) )")
    appendLine(");")
    appendLine()
    appendLine("constraint not (")
    appendLine("  ( forall (i in ROUNDS, j in ROWS, k in COLS) (isTable[i, j, k] != isTable_$i[i, j, k]) ) $AND")
    appendLine("  ( forall (i in ROUNDS, j in ROWS, k in COLS) (isDDT2[i, j, k] != isDDT2_$i[i, j, k]) )")
    appendLine(");")
    appendLine()

}

fun File.appendLine(text: String = "") = appendText(text + "\n")

private fun getStep2Solutions(step1Solutions: List<Step1Solution>, blockSize: Int, nbThreads: Int, mod: Mod, ): List<Step2Solution>? {
    val sboxTables = SboxTables(if (blockSize == 4) Sbox.SKINNY_4 else Sbox.SKINNY_8) // Only use of blockSize
    val myPool = if (nbThreads == 0) ForkJoinPool() else ForkJoinPool(nbThreads)
    var step2Solutions: List<Step2Solution> = ArrayList()
    try {
        step2Solutions = myPool.submit(Callable {
            step1Solutions.stream().parallel()
                .flatMap { solution: Step1Solution? ->
                    if (mod == Mod.SK) Step2.step2SK(
                        solution,
                        sboxTables,
                        mod,
                        2,
                        2,
                        true
                    ).stream() else Step2.optimizeEnumerateCluster(
                        solution,
                        sboxTables,
                        mod,
                        2,
                        2,
                        true
                    )
                        .stream()
                }
                .collect(Collectors.toList())
        })
            .get()
    } catch (e: InterruptedException) {
        e.printStackTrace()
        System.exit(1)
    } catch (e: ExecutionException) {
        e.printStackTrace()
        System.exit(1)
    }
    if (true) {
        if (step2Solutions.size == 0) println("No solution found") else {
            val bestProbaExponent =
                step2Solutions.stream().mapToDouble { sol: Step2Solution -> sol.probaExponent }.max().asDouble
            val bestProbaCluster =
                step2Solutions.stream().mapToDouble { sol: Step2Solution -> sol.probaClusters }.max().asDouble
            println("\nThe best probability boomerangsearch.step2 found is 2^$bestProbaExponent")
            println("The best probability the cluster found is 2^$bestProbaCluster")
        }
    }
    return step2Solutions
}
*/