package boomerangsearch;

public interface ModelFactory<T> {

    T getModel();

}
