package com.github.rloic.phd.boomerang.sandwichproba.trails;

import com.github.rloic.phd.core.cryptography.ciphers.rijndael.boomerang.Variable;
import com.github.rloic.phd.core.cryptography.attacks.boomerang.util.XorExpr;

public interface LowerTrail<Cipher> extends Trail<Cipher> {

    XorExpr getLinearExpressionAfterSbox(Variable v);

}
