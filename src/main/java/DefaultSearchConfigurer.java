import org.chocosolver.cutoffseq.LubyCutoffStrategy;
import org.chocosolver.solver.ResolutionPolicy;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.search.loop.move.MoveBinaryDFS;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainBest;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainLast;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin;
import org.chocosolver.solver.search.strategy.selectors.values.IntValueSelector;
import org.chocosolver.solver.search.strategy.selectors.variables.DomOverWDegRef;
import org.chocosolver.solver.search.strategy.selectors.variables.ImpactBased;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.tools.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.chocosolver.solver.search.strategy.Search.*;

public class DefaultSearchConfigurer {

    public static void configureSearch(Solver solver, IntVar[] dvars, int bbox, boolean free, boolean complete) {
        if (bbox > 0) {
            solver.getMove().removeStrategy();
            solver.setMove(new MoveBinaryDFS());
            switch (bbox) {
                case 1:
                    solver.setSearch(Search.domOverWDegSearch(dvars));
                    break;
                case 2:
                    solver.setSearch(new DomOverWDegRef(dvars, 0, new IntDomainBest()));
                    break;
                case 3:
                    solver.setSearch(Search.activityBasedSearch(dvars));
                    break;
                case 4:
                    ImpactBased ibs = new ImpactBased(dvars, 2, 1024, 2048, 0, false);
                    solver.setSearch(ibs);
                    break;
                case 5: {
                    IntValueSelector valueSelector;
                    if (solver.getModel().getResolutionPolicy() == ResolutionPolicy.SATISFACTION
                            || !(solver.getModel().getObjective() instanceof IntVar)) {
                        valueSelector = new IntDomainMin();
                    } else {
                        valueSelector = new IntDomainBest();
                        Solution lastSolution = new Solution(solver.getModel(), dvars);
                        solver.attach(lastSolution);
                        int[] t = new int[2];
                        valueSelector = new IntDomainLast(lastSolution, valueSelector,
                                (x, v) -> {
                                    int c = 0;
                                    for (int idx = 0; idx < dvars.length; idx++) {
                                        if (dvars[idx]
                                                .isInstantiatedTo(lastSolution.getIntVal(dvars[idx]))) {
                                            c++;
                                        }
                                    }
                                    double d = (c * 1. / dvars.length);
                                    double r = Math.exp(-t[0]++ / 25);
                                    if (solver.getRestartCount() > t[1]) {
                                        t[1] += 150;
                                        t[0] = 0;
                                    }
                                    return d > r;
                                });
                    }
                    solver.setSearch(new DomOverWDegRef(dvars, 0, valueSelector));
                }
                break;
                case 6: {
                    IntValueSelector valueSelector;
                    if (solver.getModel().getResolutionPolicy() == ResolutionPolicy.SATISFACTION
                            || !(solver.getModel().getObjective() instanceof IntVar)) {
                        valueSelector = new IntDomainMin();
                    } else {
                        valueSelector = new IntDomainBest();
                    }
                    solver.setSearch(new DomOverWDegRef(dvars, 0, valueSelector));
                }
                break;
                case 7: {
                    IntValueSelector valueSelector;
                    if (solver.getModel().getResolutionPolicy() == ResolutionPolicy.SATISFACTION
                            || !(solver.getModel().getObjective() instanceof IntVar)) {
                        valueSelector = new IntDomainMin();
                    } else {
                        valueSelector = new IntDomainBest();
                        Solution lastSolution = new Solution(solver.getModel(), dvars);
                        solver.attach(lastSolution);
                        valueSelector = new IntDomainLast(lastSolution, valueSelector, null);
                    }
                    solver.setSearch(new DomOverWDegRef(dvars, 0, valueSelector));
                }
                break;
            }
            solver.setNoGoodRecordingFromRestarts();
            solver.setNoGoodRecordingFromSolutions(dvars);
            solver.setRestarts(count -> solver.getFailCount() >= count, new LubyCutoffStrategy(500), 5000);
            solver.setSearch(lastConflict(solver.getSearch()));
            if (complete) {
                List<IntVar> otherVars = new ArrayList<>();
                for (IntVar var : solver.getModel().retrieveIntVars(true)) {
                    if (!contains(dvars, var)) {
                        otherVars.add(var);
                    }
                }
                if (!otherVars.isEmpty()) {
                    solver.setSearch(sequencer(solver.getSearch(), intVarSearch(otherVars.toArray(IntVar[]::new))));
                }
            }
        } else if (free) { // add last conflict
            solver.getMove().removeStrategy();
            solver.setMove(new MoveBinaryDFS());
            solver.setSearch(Search.defaultSearch(solver.getModel()));
            solver.setNoGoodRecordingFromRestarts();
            solver.setNoGoodRecordingFromSolutions(solver.getModel().retrieveIntVars(true));
            solver.setRestarts(count -> solver.getFailCount() >= count, new LubyCutoffStrategy(1),
                    5000);
        }
    }

    private static <T> boolean contains(T[] elements, T element) {
        for (T current : elements) {
            if (current.equals(element)) {
                return true;
            }
        }
        return false;
    }

}
